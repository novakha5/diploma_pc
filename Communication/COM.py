import threading
import time


## Thread for reading from Serial port
#
# Reads from serial port and send the complete line message to the parser
class ReadThread(threading.Thread):
    ## Initialisation thread for reading from serial port
    # @param serial_handle: Serial port handler
    # @param parser_handle: Parser thread handler
    def __init__(self, serial_handle, parser_handle):
        threading.Thread.__init__(self)
        ## The variable indicating if the thread should stop
        self.stop = False
        ## The serial port communication handler
        self.serial_handle = serial_handle
        ## The parser handler
        self.parser_handle = parser_handle

    ## The loop part of the thread
    def run(self):
        while not self.stop:
            # Check for incoming messages
            if self.serial_handle.msgIsWaiting() > 0:
                buff = self.serial_handle.readMsg()
                self.parser_handle.parse(buff)
            time.sleep(0.1)

    ## Signal the thread that it should end
    def stop_thread(self):
        self.stop = True


## Thread for sending the messages to serial port
#
# Send the message using serial port
class CmmSend(threading.Thread):
    ## Initialisation function for writing part for serial port
    # @param serial_handle: Serial port handler
    def __init__(self, serial_handle):
        threading.Thread.__init__(self)
        ## The variable indicating if the thread should stop
        self.stop = False
        ## The store place for last message
        self.lastMsg = b""
        ## Lock for thread safe data reading and writing to last message
        self.lock = threading.Lock()
        # Set to responded, since it is not waiting to anything
        ## Variable indicating if the program is waiting to any response or not
        self.waiting = 0
        ## Variable for tracking how long the message is waiting
        self.waitingTime = 0
        self.numberOfTimeouts = 0
        self.waitTill = 2
        ## Lock to thread safe handling the waiting status
        self.lockResponded = threading.Lock()
        ## The serial communication handler
        self.serial_handle = serial_handle
        self.cond = threading.Condition()
        self.paused = True

    ## The loop part of the thread
    def run(self):
        while not self.stop:
            with self.cond:
                if self.paused:
                    # In paused state, the program is not waiting for response
                    self.setWaitingStatus(0)
                    # Wait for notify to continue
                    self.cond.wait()
                    # Check if the notify is from stop_thread
                    if self.stop:
                        return
            if self.getWaitingStatus() == 1:
                self.addWaitingTime()
                if self.getWaitingTime() > self.waitTill:
                    if self.numberOfTimeouts >= 1:  # The system is not responding even with "Motors are busy" message
                        self.serial_handle.disconnect()
                        self.setWaitingStatus(0)
                        self.clearWaitingTime()
                    else:
                        self.numberOfTimeouts += 1
                        self.setWaitingStatus(0)
                        self.writeMsg(self.getLastMsg())
            time.sleep(1)

    ## Signal the thread that it should end
    def stop_thread(self):
        self.stop = True
        with self.cond:
            self.cond.notify()

    ## Set paused status to False and notify the thread
    def resume(self):
        with self.cond:
            self.paused = False
            self.cond.notify()

    ## Set paused status to True
    def pause(self):
        self.paused = True

    ## Will check if there is no message waiting for response, then send the message and save it to last one
    # @param msg: Message to send in bytearray
    # @param isManualMessage: Indication if the message is manual type form GUI
    # (Cannot wait for response from X+ and similar messages)
    # @return True if the message was sent or the connection is disabled - so it won't stall the program,
    # otherwise False
    def writeMsg(self, msg, isManualMessage=0):
        if self.getWaitingStatus() == 1:
            return False
        if not self.serial_handle.writeToSerial(msg):  # The system was disconnected
            self.setWaitingStatus(0)
            self.clearWaitingTime()
            self.updateLastMsg("")
            self.pause()
            return True
        # Save the message to last one
        self.updateLastMsg(msg)
        if isManualMessage:
            # Do not wait for response
            self.setWaitingStatus(0)
        else:
            # Set status to waiting
            self.setWaitingStatus(1)
        return True

    ## Returns the value of waiting variable
    # @return The value of waiting
    def getWaitingStatus(self):
        with self.lockResponded:
            return self.waiting

    ## Returns the value of waiting time variable
    # @return The value of waiting time
    def getWaitingTime(self):
        with self.lockResponded:
            return self.waitingTime

    ## Sets the waiting variable to given value
    # @param status: The value to be set
    def setWaitingStatus(self, status):
        with self.lockResponded:
            self.waiting = status
            self.waitingTime = 0

    ## Will increase the value of waitingTime by one
    def addWaitingTime(self):
        with self.lockResponded:
            self.waitingTime = self.waitingTime + 1

    ## Will set the value of waitingTime to 0
    def clearWaitingTime(self, changeWaitTill=2):
        with self.lockResponded:
            self.waitingTime = 0
            self.numberOfTimeouts = 0
            self.waitTill = changeWaitTill

    ## Stores last sent message
    # @param msg: The message to store
    def updateLastMsg(self, msg):
        with self.lock:
            self.lastMsg = msg

    ## Returns last sent message
    # @return The message
    def getLastMsg(self):
        with self.lock:
            return self.lastMsg
