import threading
import sys


## Class for reading from terminal input
class TerminalRead(threading.Thread):
    ## Initialisation function of the terminal input read
    # @param script_parser_handle: Handler to the parser for the commands
    def __init__(self, script_parser_handle):
        threading.Thread.__init__(self)
        ## Handler to the parser for the commands
        self.script_parser_handle = script_parser_handle
        ## The variable indicating if the thread should stop
        self.stop = False

    ## The loop part of the thread
    def run(self):
        for line in iter(sys.stdin.readline, b''):
            self.script_parser_handle.setScript(line)
            if self.stop:
                break
        sys.stdin.close()

    ## Signal the thread that it should end
    def stop_thread(self):
        self.stop = True
