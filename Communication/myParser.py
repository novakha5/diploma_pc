import time
import Position.transformPosition as tranPos


## Class for parsing information from serial port
#
# Parse incoming message, store last sent message and waiting status
class ParserClass:
    ## Initialisation function for parser class
    # @param cmm_handler: Communication - writing part handler
    # @param position_handler: Handler for the class managing current position storing
    # @param settings_handler: Handler for the settings class
    # @param steps_handler: Handler for the steps class
    def __init__(self, cmm_handler, position_handler, settings_handler, steps_handler):
        ## Write part communication handler
        self.cmm_handler = cmm_handler
        ## Current position class handler
        self.position_handler = position_handler
        ## Handler for class storing the information about HW settings
        self.settings_handler = settings_handler
        ## Handler storing the information about the steps
        self.steps_handler = steps_handler

    ## Parse the given buffer
    # @param buff: Buffer containing the message from serial port
    def parse(self, buff):
        if buff == self.cmm_handler.getLastMsg():
            # This is the desired response
            # Update the current position
            self.position_handler.updatePositionFromAsked()
            # Change the waiting status to false
            self.cmm_handler.setWaitingStatus(0)
            return
        parsed = buff.split(b',')
        length = len(parsed)
        if length != 4:
            if buff == b"Position is out of boundaries\r":
                # Error position - should not be possible after testing
                print("Given position is not in the CNC working place. Please check the settings.")
                # Unblock communication
                self.cmm_handler.setWaitingStatus(0)
            elif buff == b"The direction is not possible\r":
                # This case is in manual movement, if user is trying to move out of the boundaries
                print("Cannot move this way: the axis is on it's boundaries")
                # Unblock communication
                self.cmm_handler.setWaitingStatus(0)
            elif buff == b"Motors are busy\r":
                # This is too soon to ask another position
                self.cmm_handler.clearWaitingTime(changeWaitTill=30)
            else:
                # The problem is that the motor is still running or some communication noise - wait and send again
                time.sleep(2)
                # Unblock communication
                self.cmm_handler.setWaitingStatus(0)
                # Send the request again
                self.cmm_handler.writeMsg(self.cmm_handler.lastMsg)
        # The different position then asked was received (response to STOP, POSITION or HOME, otherwise some error)
        else:
            x = int(parsed[0])
            y = int(parsed[1])
            # Z starting position is constant given by construction
            z = int(parsed[2])
            # Fi is in relative at the beginning - it just mean it will be set to 200 steps if manual state used
            # means 0 in matrix state
            fi = int(parsed[3])
            # This is reaction to STOP message
            if self.cmm_handler.getLastMsg() == b"STOP\r":
                # Needs to update the offset for first position
                steps = self.steps_handler.returnSteps()
                changed = 0
                if steps['startX'] != x:
                    self.steps_handler.writeSteps('startX', x)
                    changed = 1
                if steps['startY'] != y:
                    self.steps_handler.writeSteps('startY', y)
                    changed = 1
                # Z starting position is constant given by construction
                # Fi is in relative at the beginning - it just mean it will be set to 200 steps if manual
                # state was used and means 0 in matrix state
                if changed:
                    self.steps_handler.updateFileSteps()
                # set the position to (1,Y_max,1,0) and state 0
                shape = self.settings_handler.getGlassShape()
                self.position_handler.setPositionSeparated(1, shape['matrixY'], 1, 0, 0)
                # Change the waiting status to false
                self.cmm_handler.setWaitingStatus(0)
            # This is reaction to POSITION message or different position then requested
            # For example because sensors were activated
            elif x == 0 and y == 0 and z == 0 and fi == 200:
                self.position_handler.setPositionSeparated(0, 0, 0, 0, 0)
                self.cmm_handler.setWaitingStatus(0)
            # The program asked for position
            elif self.cmm_handler.getLastMsg() == b"POSITION\r":
                inPos = tranPos.fromStepsToMatrix({'x': x, 'y': y, 'z': z, 'fi': fi}, self.steps_handler,
                                                  self.settings_handler.getGlassShape())
                # Check, if the position is valid
                if all(val >= 0 for val in inPos.values()):
                    self.position_handler.setPosition(inPos)
                    # Change the waiting status to false
                    self.cmm_handler.setWaitingStatus(0)
                else:
                    self.askForHome()
            else:
                inPos = tranPos.fromStepsToMatrix({'x': x, 'y': y, 'z': z, 'fi': fi}, self.steps_handler,
                                                  self.settings_handler.getGlassShape())
                if self.position_handler.compareAskedPosition(inPos):
                    # Update the current position
                    self.position_handler.updatePositionFromAsked()
                    # Change the waiting status to false
                    self.cmm_handler.setWaitingStatus(0)
                elif self.position_handler.compareCurrentPosition(inPos):
                    # The position is the same that the saved
                    # Change the waiting status to false
                    self.cmm_handler.setWaitingStatus(0)
                else:
                    print(f"The position received is not expected. ({x},{y},{z},{fi})")
                    self.askForHome()

    # Ask the STM to do the homing if the position is invalid
    def askForHome(self):
        # Change the waiting status to false
        self.cmm_handler.setWaitingStatus(0)
        # Send Homing request
        while not self.cmm_handler.writeMsg(b"HOME\r"):
            if not self.cmm_handler.systemIsConnected():
                break
            time.sleep(1)
