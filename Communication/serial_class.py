import serial
import threading


## Class for handling operations with serial port
#
# Handles connecting and disconnecting to the COM port, its communication - reading from the serial port and writing to it.
class SerialPort:
    ## Initialisation function for serial port communication
    def __init__(self):
        ## Serial port handler
        self.serPort = None
        ## The lock for thread safe reading and writing to the serial port
        self.lock = threading.Lock()
        ## Name of port to which system is connected
        self.port = ''

    ## Connect to the selected (var) COM port
    # @param var: String containing COMx value
    # @return True if the connection was established, otherwise False
    def connect(self, var):
        with self.lock:
            try:
                self.serPort = serial.Serial(port=var, baudrate=9600, bytesize=8, timeout=2, stopbits=serial.STOPBITS_ONE)
            except Exception as e:
                print("Exception: ", e)
                return False
            if self.serPort.isOpen():
                self.serPort.close()
            self.serPort.open()
            self.port = var
            return True

    ## Return information if the connection is established
    def isConnected(self):
        with self.lock:
            return self.serPort is not None

    ## Close the connection to the serial port
    def disconnect(self):
        with self.lock:
            if self.serPort is not None:
                self.serPort.close()
                self.serPort = None
                self.port = ''

    ## Return the name of port to which it is connected or '' in case of disconnected
    def returnConnectedPort(self):
        return self.port

    ## Write message (msg) to serial port
    # @param msg: Message to be sent
    # @return True if the message was sent, otherwise False
    def writeToSerial(self, msg):
        ret = True
        excepted = False
        with self.lock:
            if self.serPort is not None:
                try:
                    self.serPort.write(msg)
                except serial.serialutil.SerialTimeoutException as e:
                    print("Exception: ", e)
                    excepted = True
            else:
                ret = False
        if excepted:
            self.disconnect()
            ret = False
        return ret

    ## Check how many bytes are waiting for reading on input of serial port
    # @return Number of bytes
    def msgIsWaiting(self):
        with self.lock:
            if self.serPort is not None:
                return self.serPort.inWaiting()
            else:
                return 0

    ## Read one line from serial port - the line is ended by "\r"
    # @return String line
    def _readline(self):
        # Set the end of line character
        eol = b'\r'
        lenEol = len(eol)
        line = bytearray()
        with self.lock:
            if self.serPort is not None:
                while True:
                    c = self.serPort.read(1)
                    if c:
                        line += c
                        # check for end of line
                        if line[-lenEol:] == eol:
                            break
                    else:
                        break
        return bytes(line)

    ## Read message from serial port
    # @return Message from serial port if the communication is available, otherwise ''
    def readMsg(self):
        if self.serPort is not None:
            return self._readline()
        else:
            return ''
