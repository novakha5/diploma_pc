import enum


## Enum class for storing information about the component HW type
class HwTypes(enum.Enum):
    general = 0
    button = 1
    switch = 2
    light = 3


## Enum class for storing information about the maintained or momentary types
class LockingType(enum.Enum):
    notSpecified = 0
    maintained = 1
    momentary = 2
    maintained_momentary = 3
    momentary_maintained = 4


## Will create and store information about components placed on the testing board
class ComponentMatrix:
    ## Init function for component matrix class
    # @param settings: Handler of the settings class
    def __init__(self, settings):
        ## Handler of the settings class
        self.settings = settings
        ## Matrix containing the information about the components places on the testing board
        self.matrixOfComponents = None
        self.createMatrix()

    ## Create cleared matrix to put information about HW components
    def createMatrix(self):
        shape = self.settings.getGlassShape()
        info = {'numOfPos': 0,  # How many position it has (0 for no at all, 1 for one button, 2 for double button or
                # 2 position switch, 3 for 3 position switch, 4 for 4-way button or 4 position switch
                'distanceInZ': None,  # Position from Z0 edge in [mm]
                'xPos': (0, 0),  # Left and right side in [mm]
                'yPos': (0, 0),  # Up and down side in [mm]
                'fiPos': (0, 0),  # positions to turn in [°]
                'light': False,  # If the part is illuminated
                'lightColor': None,  # Color of the illumination if any
                'type': HwTypes.general,  # Store the type of component to determinate relevance of the next parameters
                'cupColor': None,  # Relevant only for light type of component
                'state': 0,  # Relevant only for switch type components
                'locking': LockingType.notSpecified  # Stores information about the behaviour of component
                }
        self.matrixOfComponents = [[info for _ in range(shape['matrixY'])] for _ in range(shape['matrixX'])]

    ## Put the information about the HW component to the position
    # @param x: position in X axis
    # @param y: position in y axis
    # @param info: dictionary containing information about the HW component
    def writeToMatrix(self, x, y, info):
        if self.matrixOfComponents is not None:
            self.matrixOfComponents[x-1][y-1] = info

    ## Will read and return information about the HW component on the given position
    # @param x: position in X axis
    # @param y: position in Y axis
    # @return dictionary containing information about the HW component
    def readInfo(self, x, y):
        if self.matrixOfComponents is not None:
            return self.matrixOfComponents[x-1][y-1]

    ## Give the matrix of information about stored components
    # @return the matrixOfComponents
    def getMatrix(self):
        return self.matrixOfComponents
