import tkinter as tk
from tkinter.ttk import Frame
from tkinter import messagebox as mb


## Class for frame containing buttons and option menu for serial port connection
#
# Class will create option menu with select options containing existing COM ports,
# button for refreshing the options and button for connection to the selected COM port
class ConnectionGUI(Frame):
    ## Initialisation function for communication GUI frame
    # @param parent: The master tk object handler
    # @param read_thread: Reading thread handler
    # @param parse_thread: Parsing thread handler
    # @param serial_handle: Serial communication handler
    # @param cmm_handler: Communication handler for sending the messages to serial port
    def __init__(self, parent, read_thread, parse_thread, serial_handle, cmm_handler):
        Frame.__init__(self, parent)
        ## Handler to the main tkinter window
        self.parent = parent
        # Thread and communication handlers
        ## Handler to the read thread
        self.read = read_thread
        ## Handler to parser thread
        self.parse = parse_thread
        ## Serial communication handler
        self.ser = serial_handle
        ## Communication handler for sending the messages to serial port
        self.cmm_handler = cmm_handler
        # Class variables
        ## Variable indicating that the manual position adjustment is running - should not be possible to disconnect
        self.adjusting = 0
        ## Label indicating connection to the COM port
        self.label = None
        ## Option menu handler
        self.om = None
        ## Variable to read selected value from option menu
        self.var_om = None
        ## Button "Refresh COM port list" handler
        self.button_update = None
        ## Button "Connect" handler
        self.button_connect = None
        ## List of COM ports connected to the PC
        self.com_ports = None
        # Create Label
        self.createLabel()
        # Create option menu to select COM port
        self.createOptionMenu()
        # Create buttons for connection and update of the option menu list
        self.createButtons()

    ## Find the current possible COM ports connection
    def findComports(self):
        try:
            from serial.tools.list_ports import comports
        except Exception as e:
            # No comports are connected to the pc
            print("No comports")
            print("Exception: ", e)
            return
        # If comports has been found, save their list to com_ports variable
        if comports:
            self.com_ports = comports()
        else:
            self.com_ports = None

    ## Create Label with information about COM port connection
    def createLabel(self):
        self.label = tk.Label(self, text="Not Connected")
        self.label.pack(side='top', expand='yes', fill='x', pady=5)

    ## Change the connection state of COM
    # @param connect: To which state change the information
    def connectionStateChange(self, connect):
        if connect:
            port = self.ser.returnConnectedPort()
            self.label.config(text=f"Connected to {port}")
            # Change the button to disconnect option
            self.button_connect.config(text="Disconnect")
            # Start the writing thread
            self.parse.resume()
        else:
            self.label.config(text="Not Connected")
            # Change the button to connect option
            self.button_connect.config(text="Connect")
            # Stop the writing thread
            self.parse.pause()
            self.updateComports()

    ## Create option menu and fill it with existing COM ports
    def createOptionMenu(self):
        # Create the list of existing comports
        self.findComports()
        # Create variable, which stores the selection from option menu
        self.var_om = tk.StringVar(self)
        # Put the com port list to the options in option menu
        if self.com_ports:
            comport_list = [p.device for p in self.com_ports]
            self.var_om.set(comport_list[0])
        else:
            comport_list = None

        # Create option menu
        self.om = tk.ttk.OptionMenu(self, self.var_om, *comport_list)
        self.om.configure(width=15)
        # Show the option menu
        self.om.pack(side='top', expand='yes', fill='x', pady=5)

    ## Create update and connect button
    def createButtons(self):
        self.button_update = tk.ttk.Button(self, text='Refresh COM port list', width=20, command=self.updateComports)
        self.button_update.pack(side="top", expand='yes', fill='x', pady=5)

        self.button_connect = tk.ttk.Button(self, text='Connect', width=15, command=self.connectDisconnect)
        self.button_connect.pack(side="top", expand='yes', fill='x', pady=5)

    ## Update the comports options to option menu
    def updateComports(self):
        # Clear the options in the option menu
        menu = self.om["menu"]
        menu.delete(0, "end")
        # Find out the available COM ports
        self.findComports()
        # Put the com port list to the options in option menu
        if self.com_ports:
            val_set = 0
            for p in self.com_ports:
                menu.add_command(label=p.device, command=tk._setit(self.var_om, p.device))
                if val_set == 0:
                    self.var_om.set(p.device)
                    val_set = 1

    ## Handler for the button connect so it will use the selected COM port to the serial connection
    def connectDisconnect(self):
        if not self.ser.isConnected():
            # Find out which comport is selected
            var = self.var_om.get()
            if var == '':
                print('No COM port selected')
                return
            # Connect to the serial port
            if not self.ser.connect(var):
                # Prepare message to show
                message = "Could not connect to the '{}', make sure it is the right port and the peripheral control" \
                          " board is connected.\nIf the problem persists, try restarting peripheral control board or " \
                          "unplug it and plug it again. Then repeat the connection."
                # Show information about connection problem and suggest solution
                mb.showinfo('Connection failed', message.format(var))
            else:
                print('Connected to', var)
                # Change to connected
                self.connectionStateChange(True)
                self.cmm_handler.writeMsg(b"POSITION\r")
        else:
            if not self.adjusting:
                self.ser.disconnect()
                # Change to disconnected
                self.connectionStateChange(False)
            else:
                message = "Cannot disconnect while in adjustment mode."
                mb.showinfo('Adjustment mode', message)

    ## Change the variable indicating, that the disconnect is not possible
    def disableDisconnect(self):
        self.adjusting = 1

    ## Change the variable indicating, that the disconnect is possible
    def enableDisconnect(self):
        self.adjusting = 0

    ## Returns if the system is connected to the serial port
    # @return True if the serial port is connected, otherwise False
    def systemIsConnected(self):
        return self.ser.isConnected()
