import tkinter as tk
from tkinter.ttk import Frame
from PIL import Image, ImageTk
from tkinter import messagebox as mb
import Position.transformPosition as tranPos
import time


## Class for frame, containing adjustment buttons and home button
#
# Class will create the adjustment button and the buttons with direction arrows
# It will also create the button for homing
class HomeGUI(Frame):
    ## Initialisation function for home GUI frame
    # @param parent: The master tk object handler
    # @param cmm_handler: Handler for COM writing part communication thread
    # @param connection_gui_handle: Handler for connection part of GUI
    # @param settings_handler: Handler for the settings class
    # @param steps_handler: Handler for the steps class
    # @param position_handler: Handler for current position
    def __init__(self, parent, cmm_handler, connection_gui_handle, settings_handler, steps_handler, position_handler):
        Frame.__init__(self, parent)
        ## Handler to the main tkinter window
        self.parent = parent
        ## Handler for COM writing part communication thread
        self.cmm_handler = cmm_handler
        ## Handler for connection part of GUI
        self.connection_gui_handle = connection_gui_handle
        ## Handler for the settings class
        self.settings_handler = settings_handler
        ##  Handler for the steps class
        self.steps_handler = steps_handler
        ##  Handler for current position
        self.position_handler = position_handler

        ## Button handler for Adjust home position
        self.buttonAdjust = None
        ## Variable storing the information, if the program is in adjusting position mode or not
        self.adjust = 0
        ## Button handler for sending the CNC to home position
        self.buttonHome = None

        ## Label handler for caption
        self.labelCaption = None
        ## Label handler for XY adjustment part
        self.labelXY = None
        ## Label handler for Z adjustment part
        self.labelZ = None
        ## Label handler for Fi adjustment part
        self.labelFi = None

        ## Button handler for up pointing arrow (Y axis + direction)
        self.upArrow = None
        ## Button handler for down pointing arrow (Y axis - direction)
        self.downArrow = None
        ## Button handler for left pointing arrow (X axis - direction)
        self.leftArrow = None
        ## Button handler for right pointing arrow (X axis + direction)
        self.rightArrow = None
        ## Button handler for right pointing arrow meaning forward direction (Z axis + direction)
        self.forwardArrow = None
        ## Button handler for left pointing arrow meaning backwards direction (Z axis - direction)
        self.backwardArrow = None
        ## Button handler for anticlockwise pointing arrow (Fi axis - direction)
        self.antiClockwiseArrow = None
        ## Button handler for clockwise pointing arrow (Fi axis + direction)
        self.clockwiseArrow = None

        # Create adjustment and home buttons
        self.createButtons()
        # Create the arrows buttons
        self.placeDir()

    ## Create Label for the frame and buttons for adjustment and home position
    def createButtons(self):
        self.labelCaption = tk.ttk.Label(self, text="Adjust the home position")
        self.labelCaption.grid(row=0, column=0, columnspan=5, sticky="NSEW")

        self.buttonAdjust = tk.ttk.Button(self, text="Adjust home", command=self.enableAdjust)
        self.buttonAdjust.grid(row=1, column=0, columnspan=5, sticky="NSEW")

        self.buttonHome = tk.ttk.Button(self, text="Home", command=self.home)
        self.buttonHome.grid(row=10, column=0, columnspan=5, sticky="NSEW")

    ## Callback for the adjustment button
    def enableAdjust(self):
        if not self.adjust and self.connection_gui_handle.systemIsConnected():
            shape = self.settings_handler.getGlassShape()
            info = {'numOfPos': 0,
                    # How many position it has (0 for no at all, 1 for one button, 2 for double button or
                    # 2 position switch, 3 for 3 position switch, 4 for 4-way button or 4 position switch
                    'distanceInZ': None,  # Position from Z0 edge in [mm]
                    'xPos': (0, 0),  # Left and right side in [mm]
                    'yPos': (0, 0),  # Up and down side in [mm]
                    'fiPos': (0, 0),  # positions to turn in [°]
                    'light': False,  # If the part is illuminated
                    'lightColor': None  # Color of the illumination if any
                    }
            pos = self.position_handler.getPosition()
            if pos['z'] == 2:
                pos['z'] = 1
                pos['fi'] = 0
            # Make sure it is not holding anything
            steps = tranPos.fromMatrixToSteps({'x': pos['x'], 'y': pos['y'], 'z': pos['z'], 'fi': pos['fi'], 'state': pos['state']},
                                              self.steps_handler, info, shape)
            msg = f"{steps['x']},{steps['y']},{steps['z']},{steps['fi']}\r"
            while not self.cmm_handler.writeMsg(bytes(msg, 'ascii')):
                if not self.connection_gui_handle.systemIsConnected():
                    break
                time.sleep(1)
            # Run to first position
            steps = tranPos.fromMatrixToSteps({'x': 1, 'y': shape['matrixY'], 'z': 1, 'fi': 0, 'state': 0},
                                              self.steps_handler, info, shape)
            msg = f"{steps['x']},{steps['y']},{steps['z']},{steps['fi']}\r"
            while not self.cmm_handler.writeMsg(bytes(msg, 'ascii')):
                if not self.connection_gui_handle.systemIsConnected():
                    break
                time.sleep(1)
            if self.connection_gui_handle.systemIsConnected():  # Check if the connection is still running
                self.buttonAdjust.config(text="Homed")
                self.adjust = 1
                # Disable Home button and disconnection
                self.buttonHome['state'] = tk.DISABLED
                self.connection_gui_handle.disableDisconnect()
            else:
                # Cannot do manual positioning since it is not connected to the drivers
                print("COM is not responding, disconnecting")
                self.notRespondingInfoWindow()
        # The adjustment cannot be performed because the missing connection
        elif not self.adjust:
            # Cannot do manual positioning since it is not connected to the drivers
            print("COM is not connected, cannot change position")
            self.notConnectedInfoWindow()
        else:
            self.buttonAdjust.config(text="Adjust home")
            self.adjust = 0
            # Enable home button and disconnection
            self.buttonHome['state'] = tk.NORMAL
            self.connection_gui_handle.enableDisconnect()
            # Send the position of x = 1, y = Y max, z = 1 (concealed position) and fi = 0
            steps = self.steps_handler.returnSteps()
            msg = f"{steps['startX']},{steps['startY']},{steps['startZ']},{steps['stepsFiBase']}\r"
            while not self.cmm_handler.writeMsg(bytes(msg, 'ascii')):
                if not self.connection_gui_handle.systemIsConnected():
                    break
                time.sleep(1)
            shape = self.settings_handler.getGlassShape()
            self.position_handler.setAskedPositionSeparated(1, shape['matrixY'], 1, 0, 0)

    ## Callback for the home button
    def home(self):
        if self.connection_gui_handle.systemIsConnected():
            while not self.cmm_handler.writeMsg(b"HOME\r"):
                if not self.connection_gui_handle.systemIsConnected():
                    return False
                time.sleep(1)
        else:
            # Cannot ask for home position since it is not connected to the drivers
            print("COM is not connected, cannot ask for home position")
            self.notConnectedInfoWindow()

    ## Will create info window with not connected information
    @staticmethod
    def notConnectedInfoWindow():
        message = "The driver is not connected"
        # Show information about missing connection
        mb.showinfo('Cannot send messages', message)

    ## Will create info window with not responding information
    @staticmethod
    def notRespondingInfoWindow():
        message = "The driver is not responding, disconnecting"
        # Show information about missing connection
        mb.showinfo('Cannot send messages', message)

    ## Create the Labels and arrows buttons for the adjustments
    def placeDir(self):
        # Label for X and Y axis part adjustments
        self.labelXY = tk.ttk.Label(self, text="Position in axis X,Y")
        self.labelXY.grid(row=2, column=0, columnspan=5, sticky="NSEW")

        # Load the image of up pointing arrow
        buttonImage = ImageTk.PhotoImage(Image.open('Pictures/up.png'))
        # Create the button for up pointing arrow and bind it with its callbacks
        self.upArrow = tk.ttk.Button(self, image=buttonImage)
        self.upArrow.bind("<ButtonPress-1>", self.moveUp)
        self.upArrow.bind("<ButtonRelease>", self.endMovement)
        self.upArrow.image = buttonImage
        self.upArrow.grid(row=3, column=1)

        # Load the image of left pointing arrow
        buttonImage = ImageTk.PhotoImage(Image.open('Pictures/left.png'))
        # Create the button for left pointing arrow and bind it with its callbacks
        self.leftArrow = tk.ttk.Button(self, image=buttonImage)
        self.leftArrow.bind("<ButtonPress-1>", self.moveLeft)
        self.leftArrow.bind("<ButtonRelease>", self.endMovement)
        self.leftArrow.image = buttonImage
        self.leftArrow.grid(row=4, column=0)

        # Load the image of right pointing arrow
        buttonImage = ImageTk.PhotoImage(Image.open('Pictures/right.png'))
        # Create the button for right pointing arrow and bind it with its callbacks
        self.rightArrow = tk.ttk.Button(self, image=buttonImage)
        self.rightArrow.bind("<ButtonPress-1>", self.moveRight)
        self.rightArrow.bind("<ButtonRelease>", self.endMovement)
        self.rightArrow.image = buttonImage
        self.rightArrow.grid(row=4, column=2)

        # Load the image of down pointing arrow
        buttonImage = ImageTk.PhotoImage(Image.open('Pictures/down.png'))
        # Create the button for down pointing arrow and bind it with its callbacks
        self.downArrow = tk.ttk.Button(self, image=buttonImage)
        self.downArrow.bind("<ButtonPress-1>", self.moveDown)
        self.downArrow.bind("<ButtonRelease>", self.endMovement)
        self.downArrow.image = buttonImage
        self.downArrow.grid(row=5, column=1)

        # Label for Z axis part adjustments
        self.labelZ = tk.ttk.Label(self, text="Forwards and backwards")
        self.labelZ.grid(row=6, column=0, columnspan=5)

        # Load the image of left pointing arrow meaning backwards
        buttonImage = ImageTk.PhotoImage(Image.open('Pictures/left.png'))
        # Create the button for  left pointing arrow meaning backwards and bind it with its callbacks
        self.backwardArrow = tk.ttk.Button(self, image=buttonImage)
        self.backwardArrow.bind("<ButtonPress-1>", self.moveBackwards)
        self.backwardArrow.bind("<ButtonRelease>", self.endMovement)
        self.backwardArrow.image = buttonImage
        self.backwardArrow.grid(row=7, column=0)

        # Load the image of right pointing arrow meaning forwards
        buttonImage = ImageTk.PhotoImage(Image.open('Pictures/right.png'))
        # Create the button for right pointing arrow meaning forwards and bind it with its callbacks
        self.forwardArrow = tk.ttk.Button(self, image=buttonImage)
        self.forwardArrow.bind("<ButtonPress-1>", self.moveForward)
        self.forwardArrow.bind("<ButtonRelease>", self.endMovement)
        self.forwardArrow.image = buttonImage
        self.forwardArrow.grid(row=7, column=2)

        # Label for Fi axis part adjustments
        self.labelFi = tk.ttk.Label(self, text="Clockwise and anti-clockwise")
        self.labelFi.grid(row=8, column=0, columnspan=5)

        # Load the image of clockwise pointing arrow
        buttonImage = ImageTk.PhotoImage(Image.open('Pictures/clockwise.png'))
        # Create the button for clockwise pointing arrow and bind it with its callbacks
        self.clockwiseArrow = tk.ttk.Button(self, image=buttonImage)
        self.clockwiseArrow.bind("<ButtonPress-1>", self.moveClockwise)
        self.clockwiseArrow.bind("<ButtonRelease>", self.endMovement)
        self.clockwiseArrow.image = buttonImage
        self.clockwiseArrow.grid(row=9, column=0)

        # Load the image of anticlockwise pointing arrow
        buttonImage = ImageTk.PhotoImage(Image.open('Pictures/anticlockwise.png'))
        # Create the button for anticlockwise pointing arrow and bind it with its callbacks
        self.antiClockwiseArrow = tk.ttk.Button(self, image=buttonImage)
        self.antiClockwiseArrow.bind("<ButtonPress-1>", self.moveAnticlockwise)
        self.antiClockwiseArrow.bind("<ButtonRelease>", self.endMovement)
        self.antiClockwiseArrow.image = buttonImage
        self.antiClockwiseArrow.grid(row=9, column=2)

    ## Callback for releasing direction button
    # Will send the STOP message to the driver if the system is in adjustment state
    def endMovement(self, _):
        if self.adjust:
            while not self.cmm_handler.writeMsg(b"STOP\r"):
                if not self.connection_gui_handle.systemIsConnected():
                    break
                time.sleep(0.1)

    ## Callback for clicking up direction button
    # Will send the Y+ message to the driver if the system is in adjustment state
    def moveUp(self, _):
        # This is button for Y axis in + direction
        if self.adjust:
            # It is reactive response, does not make sense to send it after some time - if there is something else
            # going on, the user will need to push the button again
            self.cmm_handler.writeMsg(b"Y+\r", 1)

    ## Callback for clicking down direction button
    # Will send the Y- message to the driver if the system is in adjustment state
    def moveDown(self, _):
        # This is button for Y axis in - direction
        if self.adjust:
            self.cmm_handler.writeMsg(b"Y-\r", 1)

    ## Callback for clicking left direction button
    # Will send the X- message to the driver if the system is in adjustment state
    def moveLeft(self, _):
        # This is button for X axis in - direction
        if self.adjust:
            self.cmm_handler.writeMsg(b"X-\r", 1)

    ## Callback for clicking right direction button
    # Will send the X+ message to the driver if the system is in adjustment state
    def moveRight(self, _):
        # This is button for X axis in + direction
        if self.adjust:
            self.cmm_handler.writeMsg(b"X+\r", 1)

    ## Callback for clicking forward direction button
    # Will send the Z+ message to the driver if the system is in adjustment state
    def moveForward(self, _):
        # This is button for Z axis in + direction
        if self.adjust:
            self.cmm_handler.writeMsg(b"Z+\r", 1)

    ## Callback for clicking backwards direction button
    # Will send the Z- message to the driver if the system is in adjustment state
    def moveBackwards(self, _):
        # This is button for Z axis in - direction
        if self.adjust:
            self.cmm_handler.writeMsg(b"Z-\r", 1)

    ## Callback for clicking clockwise direction button
    # Will send the F+ message to the driver if the system is in adjustment state
    def moveClockwise(self, _):
        # This is button for Fi axis in + direction
        if self.adjust:
            self.cmm_handler.writeMsg(b"F+\r", 1)

    ## Callback for clicking anticlockwise direction button
    # Will send the F- message to the driver if the system is in adjustment state
    def moveAnticlockwise(self, _):
        # This is button for Fi axis in - direction
        if self.adjust:
            self.cmm_handler.writeMsg(b"F-\r", 1)
