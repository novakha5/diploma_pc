import tkinter as tk
from tkinter.ttk import Frame
import numpy as np
from PIL import Image, ImageTk


## Class for GUI showing the image from the camera
#
# Class contains possibility to resize image after change of the window size
# It also contains function to update the image from the outside so it can appear as video
class ImageGUI(Frame):
    ## Initialisation function for the image show
    # @param parent: The master tk object handler
    # @param width: Width of the main window
    # @param height: Height of the main window
    # @param cam: Camera communication handler
    def __init__(self, parent, width, height, cam):
        Frame.__init__(self, parent)
        ## Handler to the main tkinter window
        self.parent = parent
        ## Camera handler
        self.camera = cam
        # Width and height of the start window
        ## Maximum width of the frame
        self.width = width
        ## Maximum height of the frame
        self.height = height
        # Class variables
        ## Image to display
        self.image = None
        ## Handler to the image object placed to canvas
        self.canvasI = None
        # Create canvas inside the sizegrip
        ## Canvas widget handler
        self.canvas = tk.Canvas(self)
        # Class variables for events
        ## X position of the press mouse button 1
        self.x1 = 0
        ## Y position of the press mouse button 1
        self.y1 = 0
        ## Handler to the rectangle object placed to canvas
        self.rectangle = None
        # Prepare to show image
        self.showImage()

    ## Takes the image from camera, rotate it to right shape and resize it to fit to the canvas widget
    # @param fromResize: Indicate update is about changing size (1) or about update video (0)
    def loadImage(self, fromResize):
        if self.camera.isConnected == 0:
            self.camera.connect()
        # Rotate image to the vertical one
        if self.camera.isConnected == 1:
            # Try to get picture from camera
            array = self.camera.getPicture()
            # If camera has been disconnected, there is None instead of numpy array
            if array is None:
                # Display no camera detected picture
                img = Image.open("Pictures/NoCamera.png")
            else:
                # Rotate numpy array picture from camera
                im = np.rot90(array, 3)
                # From the numpy array make image object
                img = Image.fromarray(im)
        else:
            # Open no camera detected picture
            if self.image is None or fromResize:
                # Display no camera detected picture
                img = Image.open("Pictures/NoCamera.png")
            else:
                # If the image about no detection is displayed and there is no new one, do not load it again
                return
        # Resize the image to fit the widget
        img.thumbnail((self.width, self.height))
        # safe image to the class variable
        self.image = ImageTk.PhotoImage(image=img)

    ## Set the canvas parameters, bind it to the resize handler and display current image on it
    def showImage(self):
        # Show canvas
        self.canvas.pack(side='top', expand='yes', fill='both')
        self.canvas.bind("<Configure>", self.resize)
        # self.canvas.bind("<ButtonPress-1>", self.mousePressCallback)
        # self.canvas.bind("<B1-Motion>", self.mouseMoveCallback)
        # self.canvas.bind("<ButtonRelease-1>", self.mouseReleaseCallback)
        # Show image in canvas
        self.updateImage(0)

    ## Handler for the resize event
    # @param event: The event variables
    def resize(self, event):
        self.width = event.width
        self.height = event.height
        self.updateImage(1)
        # Remove rectangle if there is any, since the resize will not recalculate it
        if self.rectangle is not None:
            self.canvas.delete(self.rectangle)
            self.rectangle = None

    ## Handler for mouse press event
    # @param event: The event variables
    def mousePressCallback(self, event):
        # Save starting position
        self.x1 = self.canvas.canvasx(event.x)
        self.y1 = self.canvas.canvasy(event.y)
        # If there is rectangle before, delete it
        if self.rectangle is not None:
            self.canvas.delete(self.rectangle)
            self.rectangle = None
        # Plot the rectangle only, if the camera output is displayed
        if self.camera.isConnected:
            self.rectangle = self.canvas.create_rectangle(self.x1, self.y1, self.x1, self.y1, outline="yellow", width=3)

    ## Handler for mouse motion event
    # @param event: The event variables
    def mouseMoveCallback(self, event):
        x2 = self.canvas.canvasx(event.x)
        y2 = self.canvas.canvasy(event.y)
        if self.camera.isConnected:
            # Check out of boundaries in x coord
            if x2 >= self.image.width():
                x2 = self.image.width() - 1
            elif x2 <= 0:
                x2 = 1.0
            # Check out of boundaries in y coord
            if y2 >= self.image.height():
                y2 = self.image.height() - 1
            elif y2 <= 0:
                y2 = 1.0
            # Plot rectangle
            self.canvas.coords(self.rectangle, self.x1, self.y1, x2, y2)

    ## Handler for mouse release event
    # @param event: The event variables
    def mouseReleaseCallback(self, event):
        # Save ending position
        _ = self.canvas.canvasx(event.x)
        _ = self.canvas.canvasy(event.y)
        # After completed selection, delete the rectangle
        if self.rectangle is not None:
            self.canvas.delete(self.rectangle)
            self.rectangle = None

    ## Will load current image and update it to the canvas
    # @param fromResize: Indicate update is about changing size (1) or about update video (0)
    def updateImage(self, fromResize):
        self.loadImage(fromResize)
        if self.image is not None:
            # If there is existing canvas image, reconfigure it, otherwise create it
            if self.canvasI is None:
                self.canvasI = self.canvas.create_image(0, 0, image=self.image, anchor="nw")
            else:
                self.canvas.itemconfig(self.canvasI, image=self.image)
