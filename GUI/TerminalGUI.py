import tkinter as tk
from tkinter import Text
import queue


## Class for creation of frame with textfield
#
# Will crate textfield with bound reading function to put the commands to parser
# implement queue system for write to the widget
class TerminalGUI(Text):
    ## Initialisation function for SettingsGUI class
    # @param parent: The master tk object handler
    # @param parse_handle: Handler for ParseScript class
    def __init__(self, parent, parse_handle):
        Text.__init__(self, parent, wrap=tk.WORD, height=50, width=50)
        # Bind the reading function
        self.bind("<Return>", self.get_info)
        ## Queue handler to store the text for later write to widget
        self.queue = queue.Queue()
        ## Handler to the main tkinter window
        self.parent = parent
        ## Handler for ParseScript class
        self.parse_handle = parse_handle
        # Call the repeated update of the widget
        self.update_widget()

    ## Will read given line and add it to parser
    def get_info(self, _):
        pos = self.index('end-1c linestart')
        pos = float(pos)
        line = self.get(pos, tk.END)
        # Add line to parser
        self.parse_handle.setScript(line.strip())

    # Copied from:
    # https://stackoverflow.com/questions/20303291/issue-with-redirecting-stdout-to-tkinter-text-widget-with-threads
    ## Function to update widget from queue
    def update_widget(self):
        while not self.queue.empty():
            line = self.queue.get_nowait()
            self.insert(tk.END, line)
            self.see(tk.END)
            self.update_idletasks()
        self.after(10, self.update_widget)

    # Copied from:
    # https://stackoverflow.com/questions/20303291/issue-with-redirecting-stdout-to-tkinter-text-widget-with-threads
    ## Function to add string to queue for writing
    # @param line: String to write
    def write(self, line):
        self.queue.put(line)
