from simple_pyspin import Camera, list_cameras, CameraError
from PySpin import SpinnakerException
from threading import Lock


_lock = Lock()


## Class for connecting and communication with camera and getting the pictures
#
# The camera can connect, start recording and return last picture in numpy array
class CameraCmm:
    ## Initialisation function for Camera communication
    def __init__(self):
        ## Variable indicating if camera is connected
        self.isConnected = 0
        ## Camera instance handler
        self.cam = None
        ## Boolean value informing, if the user was informed about the camera communication problem.
        # After informing, the program can be terminated.
        self.problemInformed = False
        # Create the instance of camera object
        self.connect()

    ## Will look if there is any camera to connect, if yes then it will connect to it and start the stream
    # @return True if the camera was connected, otherwise False
    def connect(self):
        # Find out if there is camera detected
        cam_list = list_cameras()
        if not cam_list.GetSize():
            return False
        try:
            # Create camera object
            self.cam = Camera()
            # Initialise camera parameters
            self.cam.init()
        except CameraError:
            # No camera detected - the camera was unplugged after the check
            print(f"Error: {CameraError}")
            return False
        # self.cam.init()
        # Set the indication of connection to connected
        self.isConnected = 1
        # Set the type of the picture which should be captured
        if self.cam.PixelFormat != "RGB8":
            self.cam.PixelFormat = "RGB8"
        self.setContinuousState()

        # Set area of interest to maximum
        if not self.setAreaOfInterest(0, self.cam.WidthMax, 0, self.cam.HeightMax):
            if not self.problemInformed:
                print("Camera could not be initialised, try unplug and plug it again.")
                self.problemInformed = True
            self.isConnected = 0
            self.closeCamera()
            return False
        self.problemInformed = False
        # Start the camera recording
        # self.cam.start()
        return True

    ## Acquire picture in the numpy array
    # @param differentExposure: Exposure time to set in case of different exposure time needs to be set
    # @return Numpy array RGB image if camera is connected, otherwise None
    def getPicture(self, differentExposure=0):
        if self.isConnected == 0:
            return None
        with _lock:
            if differentExposure:
                exp = self.getExposureTime()
                self.changeExposureTime(differentExposure)
                self.cam.stop()
            # See if the camera is recording
            if not self.cam.running:
                # Start the camera recording
                self.cam.start()
            try:
                # Make sure the camera settings has enough frame read to stabilize
                _ = self.cam.get_array()
                _ = self.cam.get_array()
                _ = self.cam.get_array()
                # Get last picture as numpy array
                ret = self.cam.get_array()
            except SpinnakerException as e:
                # This exception means that the stream has been disconnected
                # print("Exception: ", e)
                # Indicate that the camera has been disconnected
                self.isConnected = 0
                ret = None
            if differentExposure:
                self.changeExposureTime(exp)
                # Stabilize the images
                self.setContinuousState()
                try:
                    _ = self.cam.get_array()
                    _ = self.cam.get_array()
                    _ = self.cam.get_array()
                    _ = self.cam.get_array()
                except SpinnakerException as e:
                    print("Exception: ", e)
                    self.isConnected = 0
                self.cam.stop()
            return ret

    ## Stop camera recording
    def closeCamera(self):
        self.setContinuousState()
        # Stop the camera recording
        self.cam.close()
        # Indicate that the camera has been disconnected
        self.isConnected = 0

    ## Changes camera settings of exposure time
    # @param val: To which value the exposure time should be set
    def changeExposureTime(self, val):
        if val > 70784:
            val = 70784
        self.cam.GainAuto = 'Off'
        self.cam.ExposureAuto = 'Off'
        self.cam.ExposureTime = val

    ## Set the camera settings to automatic
    def setContinuousState(self):
        self.cam.GainAuto = 'Continuous'
        self.cam.ExposureAuto = 'Continuous'

    ## Return current exposure time from camera settings
    def getExposureTime(self):
        return self.cam.ExposureTime

    ## Changes camera settings of area of interest
    # @param x1: Lower x boundary
    # @param x2: Upper x boundary
    # @param y1: Lower y boundary
    # @param y2: Upper y boundary
    # @return
    def setAreaOfInterest(self, x1, x2, y1, y2):
        # check if the x1 is really lower boundary, if not swap the numbers
        if x1 > x2:
            x = x1
            x1 = x2
            x2 = x
        # check if the y1 is really lower boundary, if not swap the numbers
        if y1 > y2:
            y = y1
            y1 = y2
            y2 = y
        if x1 % 2 != 0:
            x1 -= 1
        if y1 % 2 != 0:
            y1 -= 1
        width = (x2 - x1)
        height = (y2 - y1)
        modW = width % 4
        modH = height % 4
        if modW != 0:
            width += (4 - modW)
        if modH != 0:
            height += (4 - modH)
        self.cam.stop()
        try:
            self.cam.OffsetX = 0
            # Set y offset to camera
            self.cam.OffsetY = 0
            # Calculate the width of the frame and set it to camera
            self.cam.Width = width
            # Calculate the height of the frame and set it to camera
            self.cam.Height = height
            # Set x offset to camera
            self.cam.OffsetX = x1
            # Set y offset to camera
            self.cam.OffsetY = y1
        except SpinnakerException:
            return False
        self.cam.start()
        return True
