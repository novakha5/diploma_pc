import numpy as np
import cv2
import math
import constants
from sklearn.cluster import KMeans
from PIL import Image, ImageFilter
import ComponentMatrix as compMat


## Class for processing image from camera
#
# The class will take image from camera and process it
# It can corp the image to boundaries of plexiglass
# It can return the value - light on or light off for component on requested position and in case of white cup
# it can also return the color of the light (R,G,B,W)
# It can detect the switch position (0,1,2)
class ProcessImg:
    ## Initialisation function for ProcessImg class
    # @param camera_cmm: Handler to the camera communication class
    # @param settings: Handler for system settings
    def __init__(self, camera_cmm, settings):
        ## Handler to the camera communication class
        self.camera_cmm = camera_cmm
        ## Handler for system settings
        self.settings = settings

        glass = self.settings.getGlassShape()
        # For the image, the position X and Y are switched then in the real system
        ## Number of positions in the X axis
        self.x_num = glass['matrixY']
        ## Number of positions in the Y axis
        self.y_num = glass['matrixX']
        ## Place to store the position int he image to the component in X axis
        self.x_pos = [0 for _ in range(self.x_num)]
        ## Place to store the position int he image to the component in Y axis
        self.y_pos = [0 for _ in range(self.y_num)]

        tolerance = 4
        ## Space between X positions in the pixels
        self.x_space = int(glass['spaceY'] * constants.MM_TO_PIXEL) + tolerance
        ## Space between Y positions in the pixels
        self.y_space = int(glass['spaceX'] * constants.MM_TO_PIXEL) + tolerance
        ## Value to store average brightness of classical picture and in case of low exposition, it can be checked if
        # the image returned has changed exposition correctly
        self.brightOfClassicPicture = 255
        ## Indicate if initialisation was done
        self.isInitialised = False
        ## Only relevant if initialisation was done, says if the state of switches are right
        self.initialisedRight = False

    ## To be called after camera connection, to initialise the recognition area and find tne position
    # @param matrix: Matrix with the information about the tested components
    def initWithCamera(self, matrix):
        # Find where the working area is and set area of interest to camera
        cropped = self.corpToBoundaries()
        # Take new picture and try again
        while not cropped:
            cropped = self.corpToBoundaries()
        # Find the component placement in image
        comp_identified = self.identifyPositionsInCropImg()
        # Take new picture and try again
        while not comp_identified:
            comp_identified = self.identifyPositionsInCropImg()

        glass = self.settings.getGlassShape()
        # Find out the position of the switches - expecting the robot is out of the way
        # if not, set the initialisedRight to False
        if self.initialisedRight is False:
            self.initialisedRight = True
            for index_y in range(1, glass['matrixY']+1):
                for index_x in range(1, glass['matrixX']+1):
                    comp = matrix.readInfo(index_x, index_y)
                    if comp['type'] == compMat.HwTypes.switch:
                        state = -1
                        # Try to find the switch 3 times, in case of the image problem
                        for _ in range(3):
                            state = self.stateOfSwitch(index_x, index_y, matrix)
                            if state != -1:
                                break
                        if state == -1:
                            print(f"The state of the switch on position ({index_x}, {index_y}) could no be processed.")
                            self.initialisedRight = False
        self.isInitialised = True

    ## Will try to get image two times in case of no image returned
    # @param differentExposure:  Exposure time to set in case of different exposure time needs to be set
    # @return Numpy array RGB image if camera is connected and has not returned error, otherwise None
    def getImage(self, differentExposure=0):
        img_arr = self.camera_cmm.getPicture(differentExposure)
        if img_arr is None:
            img_arr = self.camera_cmm.getPicture(differentExposure)
        if img_arr is None:
            print("Image could not be read")
            return None
        img_arr = cv2.cvtColor(img_arr, cv2.COLOR_BGR2RGB)
        return img_arr

    ## Will calculate the lower and upper boundaries for canny and create the edged image
    # @param image: The image from witch the the edges should be created
    # @param sigma: Value for the boundaries settings, default value 0.33
    # @return the edged image
    # The class was copied form
    # https://www.pyimagesearch.com/2015/04/06/zero-parameter-automatic-canny-edge-detection-with-python-and-opencv/
    @staticmethod
    def auto_canny(image, sigma=0.33):
        # compute the median of the single channel pixel intensities
        v = np.median(image)
        # apply automatic Canny edge detection using the computed median
        lower = int(max(0, (1.0 - sigma) * v))
        upper = int(min(255, (1.0 + sigma) * v))
        edged = cv2.Canny(image, lower, upper)
        # return the edged image
        return edged

    ## Will transfer picture to gray, blur it and create one with edges, then find the straight lines in it
    # @param img: BGR format image in which the lines should be found
    # @return numpy.ndarray of lines in x1, y1, x2, y2 format
    def detectLines(self, img):
        # Change to gray
        img_gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        # Blurry the picture a little - partial noise removal
        blurred = cv2.GaussianBlur(src=img_gray, ksize=(11, 11), sigmaX=11)
        # Create edged image
        edged = self.auto_canny(blurred)
        lines = cv2.HoughLinesP(edged, rho=1, theta=1 * np.pi / 180, threshold=120, minLineLength=400,
                                maxLineGap=150)
        # Return them to integer format after recalculation
        return lines.astype(int)

    ## Takes detected lines and find ot which are vertical one and which are horizontal one
    # @param lines: The detected lines in the image
    # @param tolerance: Sin of inclination which can be taken as zero (typically 1 or 2 degree)
    # @return the set of X and Y values taken as middle of vertical and horizontal lines
    @staticmethod
    def findVerticalAndHorizontal(lines, tolerance):
        # Auxiliary variables for storing possible frame liens
        storeX = []
        X = 0
        storeY = []
        Y = 0
        for index in range(len(lines)):
            # for every line read the begin and end point
            for x1, y1, x2, y2 in lines[index]:
                deltaX = abs(x1 - x2)
                deltaY = abs(y1 - y2)
                # If deltaX < deltaY then the lines are inclined in X axis, find if derivative is lover then 1 degree
                if deltaX < deltaY != 0 and deltaX / deltaY <= tolerance:
                    # Store the center value of X position
                    if x1 < x2:
                        storeX.insert(X, x1 + deltaX)
                    else:
                        storeX.insert(X, x1 - deltaX)
                    X += 1
                # If deltaX > deltaY then the lines are inclined in Y axis, find if derivative is lover then 1 degree
                elif deltaX != 0 and deltaY / deltaX <= tolerance:
                    # Store the center value of Y position
                    if y1 < y2:
                        storeY.insert(Y, y1 + deltaY)
                    else:
                        storeY.insert(Y, y1 - deltaY)
                    Y += 1
        return storeX, storeY

    ## Will take last image from camera and find the edges of plexiglass, then cut the image to its boundaries
    # @return True, if the detection was successful, otherwise False
    def corpToBoundaries(self):
        img_arr = self.getImage()
        # if img_arr is None:
        #     return False
        # Find the straight lines in picture
        lines = self.detectLines(img_arr)

        # Line directive tolerance (2 degrees)
        tolerance = math.sin(math.radians(2))
        storeX, storeY = self.findVerticalAndHorizontal(lines, tolerance)
        # If the lines was not detected on the upper part try with more tolerance - the lenses has fish eye
        if len(storeX) < 3 or len(storeY) < 3 or min(storeX) > img_arr.shape[0] / 3 or min(storeY) > \
                img_arr.shape[1] / 3:
            tolerance = math.sin(math.radians(3))
            storeX, storeY = self.findVerticalAndHorizontal(lines, tolerance)
            if len(storeX) < 3 or len(storeY) < 3 or min(storeX) > img_arr.shape[0] / 3 or min(storeY) > \
                    img_arr.shape[1] / 3:
                return False

        x1, x2 = self.findBoundaries(storeX, 0)
        y1, y2 = self.findBoundaries(storeY, 1)

        if x1 == -1 or x2 == -1 or y1 == -1 or y2 == -1:
            return False
        # pixels to add around the boundary
        boundary = 15
        x1 -= boundary
        x2 += boundary
        y1 -= boundary
        y2 += boundary

        # Set the camera Area of interest according the boundaries
        self.camera_cmm.setAreaOfInterest(np.int64(x1).item(), np.int64(x2).item(), np.int64(y1).item(),
                                          np.int64(y2).item())
        img_crop = self.getImage()
        if img_crop is not None:
            img_grey = cv2.cvtColor(img_crop, cv2.COLOR_BGR2GRAY)
            self.brightOfClassicPicture = int(sum(img_grey.sum(axis=0)) / img_grey.size)
        return True

    ## Will find boundaries in one of the axis
    # @param stored: The stored lines position which are less then 1 degree inclined
    # @param axis: For which axis the stored positions are, 0 for axis X and 1 for axis Y
    # @return Lower (b1) and upper boundary (b2)
    def findBoundaries(self, stored, axis):
        stored = np.sort(stored)
        glass = self.settings.getGlassShape()
        tol = 20
        # The image is oriented in the landscape so the Y from settings is X in image and vice versa
        if axis == 0:
            minSize = round((glass['sideY'] - constants.MM_IN_PROFILE) * constants.MM_TO_PIXEL) - tol
        else:
            minSize = round((glass['sideX'] - constants.MM_IN_PROFILE) * constants.MM_TO_PIXEL) - tol
        return self.calculateBoundaries(stored, minSize)

    ## Takes positions (X or Y) which is supposedly the same group from classification and filter out the values which
    # are too far from average, then calculate the new average
    # @param group: The positions X or Y to filter
    # @return: Filtered average
    @staticmethod
    def filterAndCalculateAverage(group):
        num_of_pix = 38  # Pixels of the profile
        average = round(sum(group) / len(group))
        # The array is sorted so we do not have to look for the values
        changed = 1
        while len(group) > 3 and changed == 1:
            changed = 0
            # Find out which value is the most problematic
            if abs(group[0] - average) > abs(group[-1] - average):
                if abs(group[0] - average) > num_of_pix:
                    group = np.delete(group, 0)
                    changed = 1
            else:
                if abs(group[-1] - average) > num_of_pix:
                    group = np.delete(group, -1)
                    changed = 1
            # Recalculate average and try again
            average = round(sum(group) / len(group))
        # Should be the lower part of the picture
        if average < 400:
            # For the lower part of the image, the value of half of the profile needs to be added
            average = average + round(num_of_pix / 2)
        else:
            # For the upper part of the image, the value of half of the profile needs to be subtracted
            average = average - round(num_of_pix / 2)
        return average

    ## Static function which will count boundaries from sorted stored line position and the minimal size in pixels
    # which should remain
    # @param stored: The sorted stored lines position which are less then 1 degree inclined
    # @param minSize: The minimal size between lower and upper boundary in pixels
    # @return Lower (b1) and upper boundary (b2)
    def calculateBoundaries(self, stored, minSize):
        # Classify the positions of stored values
        km = KMeans(n_clusters=3, random_state=0).fit(stored.reshape(-1, 1))
        previous = km.labels_[0]
        changes = []
        numOfChanges = 0
        # Find the positions in the array of stored values, where the classification changes
        for iterate in range(stored.size):
            if not km.labels_[iterate] == previous:
                previous = km.labels_[iterate]
                changes.insert(numOfChanges, iterate)
                numOfChanges += 1
        group_1 = stored[:changes[0]]
        average_1 = self.filterAndCalculateAverage(group_1)
        group_2 = stored[changes[0]:(changes[1])]
        average_2 = self.filterAndCalculateAverage(group_2)
        group_3 = stored[changes[1]:]
        average_3 = self.filterAndCalculateAverage(group_3)
        if average_2 - average_1 >= minSize:
            b1 = average_1
            b2 = average_2
        elif average_3 - average_2 >= minSize:
            b1 = average_2
            b2 = average_3
        elif average_3 - average_1 >= minSize:
            b1 = average_1
            b2 = average_3
        else:
            b1 = -1
            b2 = -1
        return b1, b2

    ## Will find circles in image and create raster of components
    def identifyPositionsInCropImg(self):
        img_arr = self.getImage()
        if img_arr is None:
            return False
        # Change to gray
        img_gray = cv2.cvtColor(img_arr, cv2.COLOR_RGB2GRAY)
        circles = cv2.HoughCircles(img_gray, cv2.HOUGH_GRADIENT, 1, 60, param1=200, param2=10, minRadius=12,
                                   maxRadius=20)
        circles = np.uint16(np.around(circles))
        # Create raster for components centers
        if circles is not None:
            # Check if the circles are inside the boundaries
            glass = self.settings.getGlassShape()
            tolerance = 20  # in pixels - contains added boundary prom cropping (15) and small tolerance
            # X and Y are switched in the image (the image is sideways)
            X_min = round((glass['sideY'] * constants.MM_TO_PIXEL) - ((glass['firstY'] + (
                    glass['spaceY'] * (glass['matrixY'] - 1))) * constants.MM_TO_PIXEL)) - tolerance
            X_max_neg = round(glass['firstY'] * constants.MM_TO_PIXEL) - tolerance
            # How  much pixels needs to be from the ending side
            # Recalculate to the value in the picture
            X_max = img_arr.shape[1] - X_max_neg
            Y_min = round((glass['sideX'] * constants.MM_TO_PIXEL) - ((glass['firstX'] + (
                    glass['spaceX'] * (glass['matrixX'] - 1))) * constants.MM_TO_PIXEL)) - tolerance
            # How  much pixels needs to be from the ending side
            Y_max_neg = round(glass['firstX'] * constants.MM_TO_PIXEL) - tolerance
            # Recalculate to the value in the picture
            Y_max = img_arr.shape[0] - Y_max_neg
            # Create the mask for X side
            mask_x = (circles[0, :, 0] >= X_min) & (circles[0, :, 0] <= X_max)
            # Create the mask for Y side
            mask_y = (circles[0, :, 1] >= Y_min) & (circles[0, :, 1] <= Y_max)
            # Combine the masks
            mask = mask_x & mask_y
            circles = circles[0, mask]
            # Classify the circles
            kmX = KMeans(n_clusters=self.x_num, random_state=0).fit(circles[:, 0].reshape(-1, 1))
            kmY = KMeans(n_clusters=self.y_num, random_state=0).fit(circles[:, 1].reshape(-1, 1))

            # Create the raster for components
            for X in range(self.x_num):
                indices = [index for index, x in enumerate(kmX.labels_) if x == X]
                self.x_pos[X] = int(sum(circles[indices, 0]) / len(indices))
            self.x_pos.sort()
            for Y in range(self.y_num):
                indices = [index for index, y in enumerate(kmY.labels_) if y == Y]
                self.y_pos[Y] = int(sum(circles[indices, 1]) / len(indices))
            self.y_pos.sort(reverse=True)
        return True

    ## Will find the component on position and return the picture of it)
    # @param image: The image, where to look for component
    # @param x: X position of the component
    # @param y: Y position of the component
    # @return Image of the requested component
    def takeOneComponent(self, image, x, y):
        # The X and Y position are switched in image!
        x1 = self.x_pos[y - 1] - int(self.x_space / 2)
        x2 = self.x_pos[y - 1] + int(self.x_space / 2)
        y1 = self.y_pos[x - 1] - int(self.y_space / 2)
        y2 = self.y_pos[x - 1] + int(self.y_space / 2)
        img = Image.fromarray(image)
        img = img.crop((x1, y1, x2, y2))
        image = np.asarray(img)
        return image

    ## Will take the image from camera witch changed exposure time.
    # In this image, will find the component and take the value of emitting light.
    # If the component has white cap, it also determinate the light color (R,G,B,W)
    # @param x: X position of the component
    # @param y: Y position of the component
    # @param isSwitch: Indicate, if the component is switch, default value is False
    # @return state of light (0,1) - (off, on), in case of problems -1
    # and color of the light if on (Red,Green,Blue,White), otherwise None
    def isLightOn(self, x, y, isSwitch=False):
        img_arr = self.getImage(differentExposure=10000)
        if img_arr is None:
            return -1, None
        img_grey = cv2.cvtColor(img_arr, cv2.COLOR_BGR2GRAY)
        bright = int(sum(img_grey.sum(axis=0)) / img_grey.size)
        while bright / self.brightOfClassicPicture > 0.60:
            img_arr = self.getImage(differentExposure=10000)
            if img_arr is None:
                return -1, None
            img_grey = cv2.cvtColor(img_arr, cv2.COLOR_BGR2GRAY)
            bright = int(sum(img_grey.sum(axis=0)) / img_grey.size)
        # Prepare return values
        isOn = 0
        ret_color = 'None'
        # Read the suitable value for component type - switch has small area threw which the lite can emmit
        # Less pixels and lower values are detected in picture for switch but in other case
        # light reflection from other sources can cause false detection especially on light type of component
        if isSwitch:
            thresh = constants.LIGHT_THRESH_SWITCH
            area = constants.LIGHT_MIN_PIXELS_SWITCH
        else:
            thresh = constants.LIGHT_TRASH
            area = constants.LIGHT_MIN_PIXELS
        # Taking one component
        img_comp = self.takeOneComponent(img_arr, x, y)
        # HSV conversion and filtration
        img_hsv = cv2.cvtColor(img_comp, cv2.COLOR_RGB2HSV)
        h, s, v = cv2.split(img_hsv)
        # Find the suitable values
        index_s = cv2.threshold(s, thresh, 255, cv2.THRESH_BINARY)[1]
        index_v = cv2.threshold(v, thresh, 255, cv2.THRESH_BINARY)[1]
        # Create mask for suitable values
        mask = ((index_s > 0) & (index_v > 0))
        # Read the color of the values
        colors = img_comp[mask]
        pixels = colors.shape[0]
        # See if the colored area is enough
        if pixels >= area:
            # Count the average of the color
            color = np.around(np.sum(colors, axis=0) / pixels).astype(int)
            isOn = 1
            # Find which color is highest
            index_of_max_color = np.where(color == np.amax(color))[0]
            if index_of_max_color == 2:
                ret_color = 'Red'
            elif index_of_max_color == 1:
                ret_color = 'Green'
            elif index_of_max_color == 0:
                ret_color = 'Blue'

        # Transform image to greyscale
        img_grey = cv2.cvtColor(img_comp, cv2.COLOR_RGB2GRAY)
        # Count white pixels (the white is so high that only value 255 needs to be counted)
        grey_cnt = np.count_nonzero(img_grey == 255)
        if grey_cnt >= area:
            isOn = 1
            # If color was detected but the white one too, try lower exposure to ensure, that the color is White and
            # not the first detected one
            if ret_color != 'None':
                control_image = self.getImage(differentExposure=5000)
                if control_image is not None:
                    control_comp = self.takeOneComponent(control_image, x, y)
                    control_grey = cv2.cvtColor(control_comp, cv2.COLOR_RGB2GRAY)
                    grey_control_cnt = np.count_nonzero(control_grey == 255)
                    if grey_control_cnt >= area:
                        ret_color = 'White'
                else:
                    # If the picture could not be caught again for validation
                    ret_color = 'White'
            else:
                ret_color = 'White'
        return isOn, ret_color

    ## Calculate the inclination from horizontal position of the lines, filter the too different values and calculate
    # the average of the angle
    # @param lines: Lines detected
    # @return: calculated angle
    @staticmethod
    def calculateAngle(lines):
        # Prepare the values for storage
        storeFi = np.array([])
        sumFi = 0
        increment = 0
        lines = lines.astype(int)
        # See all lines and calculate the Fi
        for line in lines:
            line = line[0]
            # Take higher X position as first point
            if line[0] > line[2]:
                p1 = (line[0], line[1])
                p2 = (line[2], line[3])
            else:
                p2 = (line[0], line[1])
                p1 = (line[2], line[3])
            # Find delta X
            dx = p2[0] - p1[0]
            # Find delta Y
            dy = p2[1] - p1[1]
            # Check if there is no division by 0
            if dx == 0:
                fi = 90.0
            else:
                fi = math.degrees(np.arctan(dy / dx))
            # Happens if the border of switch is cut and the remaining part is too straight
            if abs(fi) >= 82:
                continue
            # Add suitable fi to sum and to storage of fi
            sumFi += fi
            storeFi = np.append(storeFi, fi)
            increment += 1
        length = increment
        if length < 2:
            return None
        average = round(sumFi / length)
        # Too different
        while any(abs(storeFi - average) > 22):
            # Find where the differences are
            indices = np.where(abs(storeFi - average) > 22)
            if len(indices[0]) > 0:
                # Find which difference is the most problematic
                max_diff_val = storeFi[indices[0][0]]
                for index in range(len(indices[0])):
                    fi_pos = indices[0][index]
                    if fi_pos <= increment:
                        if abs(storeFi[fi_pos] - average) > abs(max_diff_val - average):
                            max_diff_val = storeFi[fi_pos]
                # Subtract this difference and cut it from the stored values
                sumFi -= max_diff_val
                length -= 1
                storeFi = np.delete(storeFi, storeFi == max_diff_val)
            if length < 2:
                return None
            # Recalculate average
            average = round(sumFi / length)
        # After all values are in the suitable range, return the average
        return average

    ## Will take the image from camera, find the component on that image and determinate the state
    # of switch component, if there is any
    # @param x: X position of the component
    # @param y: Y position of the component
    # @param matrix: To change the state accordingly in the component matrix
    # @return state of the switch (0,1,2) if there is switch component on given position, otherwise -1
    def stateOfSwitch(self, x, y, matrix):
        component = matrix.readInfo(x, y)
        img_arr = self.getImage()
        if img_arr is None:
            return -1, None
        img_comp = self.takeOneComponent(img_arr, x, y)
        img_grey = cv2.cvtColor(img_comp, cv2.COLOR_RGB2GRAY)
        # Find exact position of switch
        circles = cv2.HoughCircles(img_grey, cv2.HOUGH_GRADIENT, 1, 60, param1=200, param2=10, minRadius=15,
                                   maxRadius=18)
        # Crop around the switch
        if circles is not None and circles.shape[0] == 1:
            circles = np.uint16(np.around(circles))
            # Tolerance needs to be bigger since it sometimes detects shadows under the component instead the
            # component itself
            tol = 6
            x_center = circles[0, 0, 0]
            y_center = circles[0, 0, 1]
            radius = circles[0, 0, 2]
            x1 = max(x_center - (radius + tol), 0)
            x2 = min(x_center + (radius + tol), img_comp.shape[1])
            y1 = max(y_center - (radius + tol), 0)
            y2 = min(y_center + (radius + tol), img_comp.shape[0])
            img = Image.fromarray(img_comp)
            img_comp = img.crop((x1, y1, x2, y2))
            img_comp = np.asarray(img_comp)
        # Filtering
        result = cv2.fastNlMeansDenoisingColored(img_comp, None, 10, 10, 7, 21)
        img = Image.fromarray(result)
        # Colored edge detection
        image = img.filter(ImageFilter.FIND_EDGES)
        img_comp = np.asarray(image)
        img_grey = cv2.cvtColor(img_comp, cv2.COLOR_RGB2GRAY)
        # Find the edged circle for filtering out the outside detections
        circles = cv2.HoughCircles(img_grey, cv2.HOUGH_GRADIENT, 1, 60, param1=200, param2=10, minRadius=15,
                                   maxRadius=18)
        if circles is None or circles.shape[0] != 1:
            return -1
        # Create the circle mask
        circles = np.uint16(np.around(circles))
        center = (circles[0, 0, 0], circles[0, 0, 1])
        circle = np.zeros((img_comp.shape[0], img_comp.shape[1]), dtype="uint8")
        radius = circles[0, 0, 2]
        cv2.circle(circle, (center[0], center[1]), radius - 2, 255, -1)
        circle_mask = (circle == 255)
        # Use the mask to filter out the surroundings of the component
        img_cpy = img_comp.copy()
        img_cpy[np.logical_not(circle_mask)] = [0, 0, 0]
        img_grey = cv2.cvtColor(img_cpy, cv2.COLOR_RGB2GRAY)
        # The line detection behaves differently for lights on, the threshold need to be changed
        isOn, _ = self.isLightOn(x, y)
        if isOn:
            thresh = 25
            minLen = 10
        else:
            thresh = 19
            minLen = 9
        # Find the lines so the direction can be detected
        lines = cv2.HoughLinesP(img_grey, rho=1, theta=2 * np.pi / 180, threshold=thresh, minLineLength=minLen,
                                maxLineGap=0)
        # If no lines detected
        if lines is None:
            thresh -= 3
            minLen -= 2
            # Try once more with lover threshold
            lines = cv2.HoughLinesP(img_grey, rho=1, theta=2 * np.pi / 180, threshold=thresh, minLineLength=minLen,
                                    maxLineGap=0)
            if lines is None:
                return -1

        fi = self.calculateAngle(lines)
        # There were conflicting values and too few lines
        if fi is None:
            thresh -= 3
            minLen -= 2
            # Try once more with lover threshold
            lines = cv2.HoughLinesP(img_grey, rho=1, theta=2 * np.pi / 180, threshold=thresh, minLineLength=minLen,
                                    maxLineGap=0)
            if lines is None:
                return -1
            fi = self.calculateAngle(lines)
            if fi is None:
                return -1
        # Find out which state it is from Fi value
        if abs(fi) <= 22:
            state = 0
        elif fi > 0:
            state = 1
        else:
            state = 2
        # Write the found state to component matrix
        component['state'] = state
        matrix.writeToMatrix(x, y, component)
        return state
