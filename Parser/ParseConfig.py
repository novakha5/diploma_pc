import constants
import ComponentMatrix as compMat


## Class for parsing configuration file
class ParseConfig:
    ## Init function for config file parser
    # @param settings: Handler of the settings class
    # @param matrix: Handler of the componentMatrix class
    # @param file: String with name and the relative path to the config file
    def __init__(self, settings, matrix, file):
        ## Handler of the settings class
        self.settings = settings
        ## Handler of the componentMatrix class
        self.matrix = matrix
        ## Relative path to the config file
        self.file = file
        self.configured = self.configReading()

    ## Open and parse configuration file containing position and component
    # @return True if the file was parsed, otherwise False
    def configReading(self):
        try:
            f = open(self.file, "r")
            configString = f.read()
        except IOError:
            print("Error: Configuration file cannot be opened.")
            return False

        self.matrix.createMatrix()
        glass = self.settings.getGlassShape()

        lines = configString.split("\n")
        for line in lines:
            line = "".join(line.split())  # Remove any white spaces in the line
            if line == "":
                continue
            pos, inf = self.parsePositionLine(line)
            if all(pos) > 0 and pos[0] <= glass['matrixX'] and pos[1] <= glass['matrixY']:
                self.matrix.writeToMatrix(pos[0], pos[1], inf)
        return True

    ## Take the string of one line from config file and parse it
    # @param lineStr: The line from file
    # @return position of the component and information about it if it is valid line, otherwise None for both
    def parsePositionLine(self, lineStr):
        parts = lineStr.split(',')  # Split the line by comma
        if len(parts) < 3:
            print("Invalid line inserted: " + lineStr)
            return None, None
        info = {'numOfPos': 0,  # How many position it has (0 for no at all, 1 for one button, 2 for double button or
                # 2 position switch, 3 for 3 position switch, 4 for 4-way button or 4 position switch
                'distanceInZ': None,  # Position from Z0 edge in [mm]
                'xPos': (0, 0),  # Left and right side in [mm]
                'yPos': (0, 0),  # Up and down side in [mm]
                'fiPos': (0, 0),  # positions to turn in [°]
                'light': False,  # If the part is illuminated
                'lightColor': None,  # Color of the illumination if any
                'type': compMat.HwTypes.general,  # Store the type of component to determinate relevance of the next parameters
                'cupColor': None,  # Relevant only for light type of component
                'state': 0,  # Relevant only for switch type components
                'locking': compMat.LockingType.notSpecified  # Stores information about the behaviour of component
                }
        position = (int(parts[0]), int(parts[1]))  # Read the position, where to insert given part
        self.parsePart(parts[2], info, position)
        if len(parts) > 3 and info['light']:
            if parts[3].lower() == 'red' or parts[3].lower() == 'r':
                info['lightColor'] = 'RED'
            elif parts[3].lower() == 'green' or parts[3].lower() == 'g':
                info['lightColor'] = 'GREEN'
            elif parts[3].lower() == 'blue' or parts[3].lower() == 'b':
                info['lightColor'] = 'BLUE'
            elif parts[3].lower() == 'white' or parts[3].lower() == 'w':
                info['lightColor'] = 'WHITE'
            elif parts[3].lower() == 'rgb':
                info['lightColor'] = 'RGB'
            else:
                print("Unsupported light value: " + parts[3])
        return position, info

    ## Will parse the part string and give as many information about hte part as possible
    # @param partStr: String containing the part code
    # @param info: Dictionary where to store the information about the part
    # @param position: Position given to the part
    def parsePart(self, partStr, info, position):
        if partStr.lower() == "none":
            return
        parts = partStr.split('-')  # Split the part code by dash
        numOfParts = len(parts)  # Find out how many parts the code has
        if numOfParts < 2:
            print(f"The part code on position {position} is invalid, assuming \'None\'. Code given was: " + partStr)
            return
        if numOfParts > 2:
            info['cupColor'] = parts[2]
        part = parts[1]  # Read the relevant part of configuration
        if part[0] == 'D':
            self.getButtonParameters(part[1:], info)
        elif part[0] == 'W':
            self.getSwitchParameters(part[1:], info, position)
        elif part[0] == 'L':
            self.getLightParameters(part[1:], info)
        else:
            print(
                f"The part code is not supported, assuming \'None\' to the position {position}. Code given was: " + partStr)

    ## Take the string containing button specification and parse it
    # @param buttonStr: String containing the button specification
    # @param info: Dictionary where to store the parsed information
    @staticmethod
    def getButtonParameters(buttonStr, info):
        info['type'] = compMat.HwTypes.button
        if "DL" in buttonStr:
            # Double actuator pushbutton with indicator light
            info['numOfPos'] = 2
            info['distanceInZ'] = constants.DOUBLE_BUTTON_DIST_Z
            info['yPos'] = (constants.DOUBLE_BUTTON_Y, -1*constants.DOUBLE_BUTTON_Y)
            info['light'] = True
            info['cupColor'] = 'W'  # The cup color is always white in the middle - the position where the Light is
            ''' Possibly unuseful information - missing existing peaces to take the measurement '''
            if "F" in buttonStr:
                # Flushed
                pass
            elif "M" in buttonStr:
                # Pushbutton I and indicator light flush, pushbutton 0 extended
                pass
            else:
                # Extended
                pass
            return
        if "4" in buttonStr:
            # 4-way
            info['numOfPos'] = 4
            info['distanceInZ'] = constants.FOUR_WAY_BUTTON_DIST_Z
            info['xPos'] = (-1*constants.FOUR_WAY_BUTTON_X, constants.FOUR_WAY_BUTTON_X)
            info['yPos'] = (constants.FOUR_WAY_BUTTON_Y, -1*constants.FOUR_WAY_BUTTON_Y)
            ''' Possibly unuseful information '''
            if "I" in buttonStr:
                # Interlocked
                pass
            return
        if "L" in buttonStr:
            # Illuminated
            info['light'] = True
        if "H" in buttonStr:
            # Extended
            info['distanceInZ'] = constants.BUTTON_EXTENDED_DIST_Z
        elif "P" in buttonStr:
            # Mushroom
            info['distanceInZ'] = constants.MUSHROOM_DIST_Z
        else:
            # Flush
            info['distanceInZ'] = constants.BUTTON_FLUSH_DIST_Z
        info['numOfPos'] = 1
        if "R" in buttonStr:
            info['locking'] = compMat.LockingType.maintained
        else:
            info['locking'] = compMat.LockingType.momentary

    ## Take the string containing switch specification and parse it
    # @param switchStr: String containing the switch specification
    # @param info: Dictionary where to store the parsed information
    # @param position: Position on which the switch is placed
    @staticmethod
    def getSwitchParameters(switchStr, info, position):
        if "K" not in switchStr:
            print(
                f"Unsupported switch configuration. Assuming \'None\' on position {position}. Only thumb-up configuration is supported.")
            return
        info['type'] = compMat.HwTypes.switch
        if "R" in switchStr:
            # Maintained in case of 2 positions
            info['numOfPos'] = 2
            info['fiPos'] = (constants.MAINTAINED_FI, 0)
            info['locking'] = compMat.LockingType.maintained
        else:
            # Momentary in case of 2 positions, otherwise it will be over-written by others configurations
            info['numOfPos'] = 2
            info['fiPos'] = (constants.MOMENTARY_FI, 0)
            info['locking'] = compMat.LockingType.momentary
        info['distanceInZ'] = constants.SWITCH_DIST_Z
        if "L" in switchStr:
            # illuminated
            info['light'] = True
        if "V" in switchStr:
            # V configuration
            info['numOfPos'] = 2
            info['fiPos'] = (constants.MAINTAINED_FI, -1*constants.MAINTAINED_FI)
            info['locking'] = compMat.LockingType.maintained
            return
        if "3" in switchStr:
            # 3 positions (0 1 2)
            info['numOfPos'] = 3
            if "-1" in switchStr:
                # maintained - momentary
                info['fiPos'] = (constants.MAINTAINED_FI, -1*constants.MOMENTARY_FI)
                info['locking'] = compMat.LockingType.maintained_momentary
            elif "-2" in switchStr:
                # momentary - maintained
                info['fiPos'] = (constants.MOMENTARY_FI, -1*constants.MAINTAINED_FI)
                info['locking'] = compMat.LockingType.momentary_maintained
            else:
                info['fiPos'] = (constants.MAINTAINED_FI, -1*constants.MAINTAINED_FI)
                info['locking'] = compMat.LockingType.maintained
            return
        if "4" in switchStr:
            # 4 positions + 0 position
            info['numOfPos'] = 5
            info['fiPos'] = (constants.FOUR_WAY_FI, -1*constants.FOUR_WAY_FI)
            info['locking'] = compMat.LockingType.maintained

    ## Take the string containing light specification and parse it
    # @param lightStr: String containing the light specification
    # @param info: Dictionary where to store the parsed information
    @staticmethod
    def getLightParameters(lightStr, info):
        info['light'] = True
        info['type'] = compMat.HwTypes.light
        ''' Possibly unuseful information '''
        if "H" in lightStr:
            # Conical
            pass
        else:
            # Flush
            pass
        if "C" in lightStr:
            # Compact
            pass
