import Position.transformPosition as tP
import ComponentMatrix as compMat
import time
import threading


## Class for script parsing
#
# Script is able to recognise commands (not case sensitive):
# 'connect \<port\>' will connect to the port, if possible, for example COM1
# 'disconnect' will disconnect from the port, if the connection was established
# 'exit' will close the program
# 'open \<file name\>' will open the file, if possible and load the contents as commands for further parsing
# 'home' will send the HOME command to the connected STM, if connection is established
# 'push \<x,y\>' or 'push \<x,y,state\> or 'switch \<x,y\>' or 'switch \<x, y, state\>'
#           will push and release the button or switch and release the switch on the position if there is any with
#           given state, in case of omitted state, the program assumes 0
# 'hold <x,y>' or 'hold \<x, y,state\>' will push the button on switch the switch and hold it in the position
# 'release' will release the hold
# 'light \<x,y\>' will look if the component has possibility to have light and return 1 in case of light of the component
#           is on and 0 in case it is off, it will also return the color of the light in case of white cup of the component
# 'state \<x,y\>' in case of the switch component, returns the current position (1, 0, 2)
# The command and the parameters are separated by space
# The parameters of the commands needs to be separated by comma without any white space between them
# The class can take file name or the commandline input to parse
class ScriptParser(threading.Thread):
    ## Initialise function for script parser
    # @param serial_handle: Handler for serial class
    # @param settings_handle: Handler for settings class
    # @param steps_handle: Handler for steps class
    # @param component_matrix: Matrix of information about current HW components
    # @param com_handle: Handler for write part of serial communication
    # @param current_position_handle: Handler for current position class
    # @param proc_img_handle: Handler for image processing class
    def __init__(self, serial_handle, settings_handle, steps_handle, component_matrix, com_handle,
                 current_position_handle, proc_img_handle):
        threading.Thread.__init__(self)
        ## Handler for serial class
        self.serial_handle = serial_handle
        ## Handler for settings class
        self.settings_handle = settings_handle
        ## Handler for steps class
        self.steps_handle = steps_handle
        ## Matrix of information about current HW components
        self.component_matrix = component_matrix
        ## Handler for write part of serial communication
        self.com_handle = com_handle
        ## Handler for current position class
        self.current_position_handle = current_position_handle
        ## Script for parsing (Line or whole file)
        self.script = ""
        ## Handler for image processing class
        self.proc_img_handle = proc_img_handle
        ## The variable indicating if the thread should stop
        self.stop = False
        ## mutex for writing to script string
        self.mutex = threading.Lock()

    ## The loop part of the thread
    def run(self):
        while not self.stop:
            if self.script:
                self.parseScript()
            time.sleep(0.5)

    ## Signal the thread that it should end
    def stop_thread(self):
        self.stop = True

    ## Read the script from file
    # param file: file name
    def openScript(self, file):
        try:
            f = open(file, "r")
            with self.mutex:
                self.script += f.read()
        except IOError:
            print("Error: Script file cannot be opened.")

    ## Parse the loaded script
    # @return True if script was parsed, otherwise False
    def parseScript(self):
        ret = True
        if not self.script:
            return None
        with self.mutex:
            lines = self.script.split("\n")
            # Clear the parsed script - in case that another command comes while the parser is running
            self.script = ""
        # Parse all lines in script
        for line in lines:
            # The program should exit
            if self.stop:
                break
            if not self.parseLine(line) and not self.serial_handle.isConnected():
                ret = False
        return ret

    ## Set the script to given string
    # @param script: The string for parsing
    def setScript(self, script):
        if script is not None:
            with self.mutex:
                self.script += script

    ## Parse one line of the script
    # @param line: String containing one line
    # @return True if the command was processed, otherwise False (in case of incorrectly entered parameters)
    def parseLine(self, line):
        # Convert the line to lowercase
        line = line.lower()
        # Split it by space
        parts = line.split(" ")
        # If there are any empty strings in the parts, remove them
        while "" in parts:
            parts.remove("")
        # Find out how many parts were created from the split
        length = len(parts)
        if length == 0:
            # Empty command
            return True
        if "connect" == parts[0]:
            # Connection seeds to have one parameter (command and the port where to connect)
            if length == 2:
                if not self.serial_handle.isConnected():
                    # If the system is not connected try to connect it
                    if not self.serial_handle.connect(parts[1].upper()):
                        print("Could not connect to " + parts[1].upper())
                        return False
                    else:
                        print(f"The system connected to {parts[1].upper()}")
                        self.com_handle.resume()
                        self.com_handle.writeMsg(b"POSITION\r")
                        return True
                else:
                    print("System is already connected")
                    return True
            else:
                print("The \'connect\' command takes one parameter")
                return False
        elif "open" in parts[0]:
            # Open command takes one parameter containing file name and path if it is not inserted to dictionary
            if length == 2:
                self.openScript(parts[1])
        elif "exit" in parts[0]:
            # End the thread - the main program will shut everything else
            self.stop_thread()
        elif self.serial_handle.isConnected():
            # The commands which need to use the connection to the STM
            if "push" in parts[0] or "switch" in parts[0] or "hold" in parts[0]:
                # push command take parameters x,y or x,y,state without he space!
                if length != 2:
                    print(f"The \'{parts[0]}\' command takes two or three arguments")
                    return False
                return self.parsePush(parts[1], parts[0])
            elif "release" in parts[0]:
                return self.release()
            elif "light" in parts[0]:
                # Need to check, if the system for image recognition is initialised
                if not self.proc_img_handle.isInitialised:
                    print("The camera recognition has not been initialised yet. The camera may not be connected.")
                    return False
                # light takes x,y parameter without the space!
                if length != 2:
                    print(f"The \'{parts[0]}\' command takes two arguments")
                    return False
                x, y, _ = self.parsePositionAndState(parts[1], parts[0], True)
                state, color = self.getLightState(x, y)
                if state < 0 or state > 1:
                    # unsupported or invalid states
                    return False
                if state == 1:
                    st = 'On'
                else:
                    st = 'Off'
                # If color is relevant, print it too
                if color is not None:
                    print(f"({x}, {y}) light: {st}, {color}")
                else:
                    print(f"({x}, {y}) light: {st}")
            elif "state" in parts[0]:
                # Need to check, if the system for image recognition is initialised
                if not self.proc_img_handle.isInitialised:
                    print("The camera recognition has not been initialised yet. The camera may not be connected.")
                    return False
                # State takes x,y parameters without the space!
                if length != 2:
                    print(f"The \'{parts[0]}\' command takes two arguments")
                    return False
                x, y, _ = self.parsePositionAndState(parts[1], parts[0])
                if x < 0 or y < 0:
                    return False
                state = self.getSwitchState(x, y)
                if state < 0 or state > 4:
                    # Not relevant or invalid state
                    return False
                print(f"({x}, {y}) state: {state}")
            elif "home" in parts[0]:
                # Send the home request
                while not self.com_handle.writeMsg(b"HOME\r"):
                    if self.serial_handle is None:
                        return False
                    time.sleep(1)
                if not self.waitForResponse():
                    return False
            elif "disconnect" in parts[0]:
                # Disconnect from port
                self.serial_handle.disconnect()
                # Pause the serial parser
                self.com_handle.pause()
                print("The system is disconnected")
            elif parts[0] == '':  # Empty command
                pass
            else:
                # Unsupported command
                print(f"Command \'{parts[0]}\' is not supported.")
                return False
            return True
        else:
            # Inform user in first command that the connection is not established
            print("System is not connected, cannot run the commands.")
            return False

    ## Waits till waiting status is cleared
    # @return True if the response come, False if the serial communication was broken
    def waitForResponse(self):
        while self.com_handle.getWaitingStatus() == 1:
            if self.serial_handle is None:
                return False
            time.sleep(0.5)
        return True

    ## Check if there is no waiting message, if yes then waits, then run release
    def checkWaitingStatusAndZPosition(self):
        if self.com_handle.getWaitingStatus() == 1:
            print("Waiting for previous command to finish.")
            # Give time before asking if completed
            time.sleep(2)
            # Wait for the response
            if not self.waitForResponse():
                return False
        return self.release()

    ## Takes current position and if the z = 2, then send request for the same position with z = 1
    # @return True if completed, otherwise false
    def release(self):
        ret = True
        # Find out if the current position is in release state
        pos = self.current_position_handle.getPosition()
        if pos['z'] == 2:
            pos['z'] = 1
            pos['fi'] = 0
            # If the state is not in released state, run to it
            ret = self.sendPositionAndWaitForResponse(pos)
        return ret

    ## Parse position and state of push command and execute it
    # @param parts: Parameters given with command
    # @param comm: The command name
    # @return True if the command was processed, otherwise False (in case of incorrectly entered parameters)
    def parsePush(self, parts, comm):
        x, y, stateOrFi = self.parsePositionAndState(parts, comm)
        if x < 0 or y < 0 or stateOrFi < 0:
            return False
        component = self.component_matrix.readInfo(x, y)
        # In case of switch, the system needs to have initialised image recognition - to know the switch positions
        if not self.proc_img_handle.isInitialised and component['type'] == compMat.HwTypes.switch:
            print("The camera recognition has not been initialised yet - the state of switches is unknown. "
                  "The camera may not be connected.")
            return False
        elif not self.proc_img_handle.initialisedRight:
            if not self.doInitStates():
                print("The image recognition initialisation of switch states could not be performed.")
                return False
        self.checkWaitingStatusAndZPosition()
        # Hold part of the execution
        if component['type'] == compMat.HwTypes.button:
            ret = self.holdButton(x, y, stateOrFi)
        elif component['type'] == compMat.HwTypes.switch:
            # switch component
            ret = self.holdSwitch(x, y, stateOrFi, self.component_matrix)
        else:
            print(f"Component on position ({x}, {y}), does not support \'{comm}\' command")
            ret = False
        # Release part of the execution
        if "hold" not in comm and ret:
            # It is push or switch command
            ret = self.release()
        return ret

    ## Will calculate the steps position and send the request then wait for response
    # @param x: X matrix position
    # @param y: Y matrix position
    # @param state: State position (0, 1, 2, 3 or 4)
    # @return True when completed
    def holdButton(self, x, y, state):
        # Create matrix position
        matrix = {'x': x, 'y': y, 'z': 2, 'fi': 0, 'state': state}
        return self.sendPositionAndWaitForResponse(matrix)

    ## Will find out the state of switch, then hold it and switch it
    # @param x: X matrix position
    # @param y: Y matrix position
    # @param fi: Desired Fi position
    # @param matrix: To change the state accordingly in the component matrix
    # @return True when completed, False if the state of switch does not need to be changed
    def holdSwitch(self, x, y, fi, matrix):
        component = matrix.readInfo(x, y)
        # Preparing fi
        fiNow = component['state']
        # Check if the switch position needs to be changed
        if fiNow == fi:
            print("The state of the switch is desired one.")
            return False
        # Create matrix position
        matrix_pos = {'x': x, 'y': y, 'z': 1, 'fi': fiNow, 'state': 0}
        if not self.sendPositionAndWaitForResponse(matrix_pos):
            return False

        # Hold and rotate switch (the STM does z before fi)
        # Create matrix position
        matrix_pos = {'x': x, 'y': y, 'z': 2, 'fi': fi, 'state': 0}
        if not self.sendPositionAndWaitForResponse(matrix_pos):
            return False

        # Update the info of component
        if component['locking'] == compMat.LockingType.maintained or \
                (component['locking'] == compMat.LockingType.maintained_momentary and (fi == 1 or fi == 0)) or \
                (component['locking'] == compMat.LockingType.momentary_maintained and (fi == 2 or fi == 0)):
            component['state'] = fi
            matrix.writeToMatrix(x, y, component)
        return True

    ## Find out if the component with y position is visible, if not, move the robot out ot the way
    # @param y: Y position of the component which needs to be visible
    def moveOutOfTheImage(self, y):
        # Calculate if the position is visible
        currPos = self.current_position_handle.getPosition()
        y_need = -1
        # If the robot is homed in y position, there is no need for movement
        if currPos['y'] != 0:
            if y >= 5:
                if not ((y + 1) < currPos['y'] or (y - 4) > currPos['y']):
                    # Find which is closer
                    if abs((y - 4) - currPos['y']) < abs((y + 1) - currPos['y']):
                        # Go up
                        y_need = y - 4
                    else:
                        # Go down
                        if y == self.settings_handle.getGlassShape()['matrixY']:
                            # If the position is last, the lower position is homed for y
                            y_need = 0
                        else:
                            y_need = y + 1
            elif y == 1:
                if (y + 3) > currPos['y']:
                    # Go down
                    y_need = y + 3
            else:  # y = 2, 3 or 4
                if (y + 2) > currPos['y']:
                    # Go down
                    y_need = y + 2
        if y_need != -1:
            # Movement is needed
            matrix_pos = {'x': currPos['x'], 'y': y_need, 'z': currPos['z'], 'fi': currPos['fi'], 'state': 0}
            return self.sendPositionAndWaitForResponse(matrix_pos)
        return True

    ## Looks to the position of the component, finds if the light is on or off
    # @param x: X position in component matrix
    # @param y: Y position in component matrix
    # @return 0 if light is off, 1 if light is on, -1 in case of problem processing the command
    # and color if relevant, otherwise None
    def getLightState(self, x, y):
        component = self.component_matrix.readInfo(x, y)
        if not component['light']:
            print(f"The component on position ({x}, {y}) does not have light.")
            return -1, None
        # Check if there is anything to wait for to be sure the position is valid
        if not self.checkWaitingStatusAndZPosition() or not self.moveOutOfTheImage(y):
            return -1, None
        # Process image
        isOn, color = self.proc_img_handle.isLightOn(x, y, isSwitch=(component['type'] == compMat.HwTypes.switch))
        # Color is relevant only in case of the white cup, otherwise the color can be different then what is
        # captured on camera
        # The color is not relevant if the light is off
        if component['cupColor'] != 'W' or isOn == 0:
            color = None
        return isOn, color

    ## Finds out if camera can see the position, if not move the CNC, capture and process image
    # @param x: matrix position
    # @param y: matrix position
    # @return State of the switch, or -1 if the command could not be processed
    def getSwitchState(self, x, y):
        component = self.component_matrix.readInfo(x, y)
        if component['type'] != compMat.HwTypes.switch:
            print(f"The component on position ({x}, {y}) is not switch type.")
            return -1
        # Check if there is anything to wait for to be sure the position is valid and then check if the camera can see
        # the component, if not, move out of the way
        if not self.checkWaitingStatusAndZPosition() or not self.moveOutOfTheImage(y):
            return -1, None
        state = -1
        # Process image if there is any problem with recognition (for example capturing image), try 3 times
        for _ in range(3):
            state = self.proc_img_handle.stateOfSwitch(x, y, self.component_matrix)
            if state != -1:
                break
        if state == -1:
            print("The state of the switch could no be processed.")
        return state

    ## Will send the message with desired position and wait till response come
    # @param position: Position, where to move
    # @return True if the send was successful, otherwise False
    def sendPositionAndWaitForResponse(self, position):
        # Calculate steps position
        steps = tP.fromMatrixToSteps(position, self.steps_handle,
                                     self.component_matrix.readInfo(position['x'], position['y']),
                                     self.settings_handle.getGlassShape())
        # Check the validity of returned steps
        if any(value < 0 for value in steps.values()):
            print("Position is invalid.")
            return False
        # Create message
        msg = f"{steps['x']},{steps['y']},{steps['z']},{steps['fi']}\r"
        if not self.serial_handle.isConnected():
            print("The system is not connected to motor drivers.")
            return False
        # Send the message
        while not self.com_handle.writeMsg(bytes(msg, 'ascii')):
            if self.serial_handle is None:
                return False
            time.sleep(0.5)
        # Store the asked position
        self.current_position_handle.setAskedPosition(position)
        # Give time before asking if completed
        time.sleep(1)
        return self.waitForResponse()

    ## Takes the arguments and read the position and state, check if the given arguments are valid
    # @param arguments: The string containing arguments part of the command
    # @param comm: Which command was used
    # @param isLightCommand: Information if the state is invalid, since there is no physical interaction
    # @return x, y and state values if valid, otherwise -1, -1, -1
    def parsePositionAndState(self, arguments, comm, isLightCommand=False):
        pos = arguments.split(',')
        numOfArgs = len(pos)
        # Check if there is right number of arguments
        if numOfArgs < 2 or numOfArgs > 3 or (numOfArgs == 3 and ('light' in comm or 'state' in comm)):
            if 'light' in comm or 'state' in comm:
                print(f"The \'{comm}\' command takes two arguments")
            else:
                print(f"The \'{comm}\' command takes two or three arguments")
            return -1, -1, -1
        if pos[0].isnumeric() and pos[1].isnumeric():
            x = int(pos[0])
            y = int(pos[1])
        else:
            print(f"The \'{comm}\' command takes two numeric arguments separated by comma")
            return -1, -1, -1
        glass = self.settings_handle.getGlassShape()
        # Check if the position is in the current glass shape
        if x < 1 or y < 1 or x > glass['matrixX'] or y > glass['matrixY']:
            print(f"The position of \'{comm}\' command is invalid. The position given: ({x}, {y})")
            return -1, -1, -1
        state = 0
        component = self.component_matrix.readInfo(x, y)
        if numOfArgs == 3 and pos[2].isnumeric():
            state = int(pos[2])
        # For light command, the state is invalid but the system returns 0
        if not isLightCommand:
            # State cannot be less then zero
            if state < 0:
                print(f"Tha state of \'{comm}\' command is invalid for component on position ({x}, {y}). State given: {state}.")
                return -1, -1, -1
            # Switch states are always lower then number of positions
            if component['type'] == compMat.HwTypes.switch and state >= component['numOfPos']:
                print(f"Tha state of \'{comm}\' command is invalid for component on position ({x}, {y}). "
                      f"State given: {state}.")
                return -1, -1, -1
            # Buttons states
            if component['type'] == compMat.HwTypes.button:
                # The double and 4-way buttons does not have state 0
                if state == 0 and component['numOfPos'] > 1:
                    print(f"Tha state of \'{comm}\' command is invalid for given HW component on position ({x}, {y}). "
                          f"State given: {state}")
                    return -1, -1, -1
                if component['numOfPos'] == 1 and state != 0:
                    print(f"Tha state of \'{comm}\' command is invalid for given HW component on position ({x}, {y})."
                          f" State given: {state}")
                    return -1, -1, -1
        return x, y, state

    ## Will go threw all switches and actualize their state
    def doInitStates(self):
        print("The system has uninitialised one or more switches states, doing initialisation.")
        if not self.checkWaitingStatusAndZPosition():
            return False
        # Move out of the way
        currPos = self.current_position_handle.getPosition()
        matrix_pos = {'x': currPos['x'], 'y': 0, 'z': currPos['z'], 'fi': currPos['fi'], 'state': 0}
        if not self.sendPositionAndWaitForResponse(matrix_pos):
            return False
        init = True
        glass = self.settings_handle.getGlassShape()
        for index_y in range(1, glass['matrixY'] + 1):
            for index_x in range(1, glass['matrixX'] + 1):
                comp = self.component_matrix.readInfo(index_x, index_y)
                if comp['type'] == compMat.HwTypes.switch:
                    state = -1
                    for _ in range(3):
                        state = self.proc_img_handle.stateOfSwitch(index_x, index_y, self.component_matrix)
                        if state != -1:
                            break
                    if state == -1:
                        print(f"The state of the switch on position ({index_x}, {index_y}) could no be processed.")
                        init = False
        if init:
            self.proc_img_handle.initialisedRight = True
            print("Initialisation complete.")
        return init
