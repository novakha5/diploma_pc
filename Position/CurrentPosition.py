from threading import Lock

_mutex = Lock()
_askedMutex = Lock()


## Class will store and handle the current position of the system in relative (matrix) coordinates
class CurrentPosition:
    ## Initial function for CurrentPosition class
    # @param settings_handler: Handler for the settings class in constants
    def __init__(self, settings_handler):
        ## Handler for the system HW settings
        self.settings_handler = settings_handler
        ## Protected value of current position - need to use getter and setter to see and change
        self._position = {"x": 0, "y": 0, "z": 0, "fi": 0, "state": 0}
        ## Protected value with position asked by USB (before the response come)
        self._askedPosition = {"x": -1, "y": -1, "z": -1, "fi": -1, "state": -1}

    ## Getter for position structure
    # @return current position
    def getPosition(self):
        with _mutex:
            return self._position

    ## Setter for position structure
    # @param x: Position x if the value should be set, otherwise -1
    # @param y: Position y if the value should be set, otherwise -1
    # @param z: Position z if the value should be set, otherwise -1
    # @param fi: Position fi if the value should be set, otherwise -1
    # @param state: Position state if the value should be set, otherwise -1
    def setPositionSeparated(self, x, y, z, fi, state):
        self.setPosition({'x': x, 'y': y, 'z': z, 'fi': fi, 'state': state})

    ## Setter for position structure using dictionary
    # @param pos: dictionary containing position to set
    def setPosition(self, pos):
        keys = pos.keys()
        with _mutex:
            if 'x' in keys and 0 <= pos['x'] <= self.settings_handler.getGlassShape()['matrixX']:
                self._position['x'] = pos['x']
            if 'y' in keys and 0 <= pos['y'] <= self.settings_handler.getGlassShape()['matrixY']:
                self._position['y'] = pos['y']
            if 'z' in keys and 0 <= pos['z'] <= 2:
                self._position['z'] = pos['z']
            if 'fi' in keys and 0 <= pos['fi'] <= 4:
                self._position['fi'] = pos['fi']
            if 'state' in keys and 0 <= pos['state'] <= 4:
                self._position['state'] = pos['state']

    ## Return the value of the position which was last sent to the Control board, if the response didn't come yet,
    # otherwise it returns invalid position
    # @return the position, which was sent to the Control board
    def getAskedPosition(self):
        with _askedMutex:
            return self._askedPosition

    ## Setter for asked position structure
    # @param x: Position x if the value should be set, otherwise -1
    # @param y: Position y if the value should be set, otherwise -1
    # @param z: Position z if the value should be set, otherwise -1
    # @param fi: Position fi if the value should be set, otherwise -1
    # @param state: Position state if the value should be set, otherwise -1
    def setAskedPositionSeparated(self, x, y, z, fi, state):
        self.setAskedPosition({'x': x, 'y': y, 'z': z, 'fi': fi, 'state': state})

    ## Setter for asked position structure using dictionary
    # @param pos: dictionary containing position to set
    def setAskedPosition(self, pos):
        keys = pos.keys()
        with _askedMutex:
            if 'x' in keys and 0 <= pos['x'] <= self.settings_handler.getGlassShape()['matrixX']:
                self._askedPosition['x'] = pos['x']
            if 'y' in keys and 0 <= pos['y'] <= self.settings_handler.getGlassShape()['matrixY']:
                self._askedPosition['y'] = pos['y']
            if 'z' in keys and 0 <= pos['z'] <= 2:
                self._askedPosition['z'] = pos['z']
            if 'fi' in keys and 0 <= pos['fi'] <= 4:
                self._askedPosition['fi'] = pos['fi']
            if 'state' in keys and 0 <= pos['state'] <= 4:
                self._askedPosition['state'] = pos['state']

    ## Sets asked position to the invalid state
    def clearAskedPosition(self):
        with _askedMutex:
            self._askedPosition = {"x": -1, "y": -1, "z": -1, "fi": -1, "state": -1}

    ## Returns true, if asked position is set, otherwise false
    def isAskedPositionValid(self):
        with _askedMutex:
            return self._askedPosition['x'] != -1 and self._askedPosition['y'] != -1 and self._askedPosition['z'] != -1 \
                   and self._askedPosition['fi'] != -1 and self._askedPosition['state'] != -1

    ## Takes asked position and moves it to current one, clears the asked one
    def updatePositionFromAsked(self):
        if self.isAskedPositionValid:
            with _askedMutex:
                with _mutex:
                    self._position = self._askedPosition
            self.clearAskedPosition()

    # Compare given position with the current one
    # @param pos: dictionary containing position which should be compared
    # @return true if the dictionaries containing positions are the same, otherwise false
    def compareCurrentPosition(self, pos):
        current = self.getPosition()
        return current == pos

    # Compare given position with the asked one
    # @param pos: dictionary containing position which should be compared
    # @return true if the dictionaries containing positions are the same, otherwise false
    def compareAskedPosition(self, pos):
        asked = self.getPosition()
        return asked == pos
