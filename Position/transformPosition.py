import constants


## Transform matrix position to the steps needed from home position
# @param matrix: Dictionary containing keys x, y, z and fi of position and
# state which means the position in respect to the component
# 0 is center, 1 is upper, 2 is lower, 3 is left, 4 is right
# @param steps:  Handler to the steps class instance
# @param componentInfo: Information about the component on the given position
# @param glassShape: Glass shape dictionary containing the information about the HW state of system
# @return Dictionary containing keys x, y, z and fi of steps
def fromMatrixToSteps(matrix, steps, componentInfo, glassShape):
    ret = {'x': -1, 'y': -1, 'z': -1, 'fi': -1}
    # Check if received matrix position has all axes
    if ('x' not in matrix) or ('y' not in matrix) or ('z' not in matrix) or ('fi' not in matrix) or \
            ('state' not in matrix):
        print("The position has not all the values")
        return ret
    # Check if the component information has all needed pats for calculation
    if('numOfPos' not in componentInfo) or ('distanceInZ' not in componentInfo) or ('xPos' not in componentInfo)\
            or ('yPos' not in componentInfo) or ('fiPos' not in componentInfo):
        print("The information about component is not complete")
        return ret
    # Count the transformation to steps position
    # state means the position in respect to the component the
    # 0 is center, 1 is upper, 2 is lower, 3 is left, 4 is right
    ret['x'] = countXSteps(matrix['x'], steps, componentInfo, matrix['state'])
    ret['y'] = countYSteps(matrix['y'], steps, componentInfo, matrix['state'], glassShape)
    ret['z'] = countZSteps(matrix['z'], steps, componentInfo)
    ret['fi'] = countFiSteps(matrix['fi'], steps, componentInfo)
    if not all(val >= 0 for val in ret.values()):
        return {'x': -1, 'y': -1, 'z': -1, 'fi': -1}
    # Debug assert for calculating the steps
    assert matrix == fromStepsToMatrix(ret, steps, glassShape)
    return ret


## Count steps needed to do from home position to the matrix position in X axis
# @param val: Which position needs to be transformed
# @param steps: Handler to the steps class instance
# @param info: Information about the component on given place
# @param state: The position in respect to the component the
# 0 is center, 1 is upper, 2 is lower, 3 is left, 4 is right
# @return Number of steps if valid settings, otherwise -1
def countXSteps(val, steps, info, state):
    # Invalid position
    if val < 0 or state > info['numOfPos']:
        return -1
    # Home position
    if val == 0:
        return 0
    # Shift in regards of center of the component in [mm]
    shift = 0
    if state == 3:
        shift = info['xPos'][0]
    elif state == 4:
        shift = info['xPos'][1]
    stepsVar = steps.returnSteps()
    # Offset of first HW component from home + position * steps between the next HW component
    # + shift in regards of the component
    return stepsVar['startX'] + (val - 1) * stepsVar['stepsX'] + shift * stepsVar['stepsFromMmBase']


## Count steps needed to do from home position to the matrix position in Y axis
# @param val: Which position needs to be transformed
# @param steps: Handler to the steps class instance
# @param info: Information about the component on given place
# @param state: The position in respect to the component the
# 0 is center, 1 is upper, 2 is lower, 3 is left, 4 is right
# @param glassShape: Glass shape dictionary containing the information about the HW state of system
# @return Number of steps if valid settings, otherwise -1
def countYSteps(val, steps, info, state, glassShape):
    # Invalid position
    if val < 0 or state > info['numOfPos']:
        return -1
    # Home position
    if val == 0:
        return 0
    # The Y axis needs to transform to have 1 in the top left corner (but 0 is home position in the left corner)
    val = glassShape['matrixY'] - (val - 1)
    # Shift in regards of center of the component in [mm]
    shift = 0
    if state == 1:
        shift = info['yPos'][0]
    elif state == 2:
        shift = info['yPos'][1]
    stepsVar = steps.returnSteps()
    # Offset of first HW component from home + position * steps between the next HW component
    # + shift in regards of the component
    return stepsVar['startY'] + (val - 1) * stepsVar['stepsY'] + shift * stepsVar['stepsFromMmBase']


## Count steps needed to do from home position to the matrix position in Z axis
# @param val: Which position needs to be transformed
# @param steps: Handler to the steps class instance
# @param info: Information about the component on given place
# @return Number of steps if valid settings, otherwise -1
def countZSteps(val, steps, info):
    # There are only 3 states of the Z (0, 1, 2)
    # 0 Home position
    # 1 The concealed state
    # 2 Push button or grip switch
    if val == 0:
        return 0
    stepsVar = steps.returnSteps()
    if val == 1:
        return stepsVar['startZ']
    # Invalid position
    if val < 0 or val > 2 or info['distanceInZ'] is None or info['numOfPos'] == 0:
        return -1
    # distance from Z concealed state * steps between the states + concealed state
    return int(info['distanceInZ'] * stepsVar['stepsFromMmBase']) + stepsVar['startZ']


## Count steps needed to do from home position to the matrix position in Fi axis
# @param val: Which position needs to be transformed
# @param steps: Handler to the steps class instance
# @param info: Information about the component on given place
# @return Number of steps if valid settings, otherwise -1
def countFiSteps(val, steps, info):
    # There are only 5 values of the Fi (0, 1, 2, 3, 4)
    stepsVar = steps.returnSteps()
    # Home position
    if val == 0:
        return stepsVar['stepsFiBase']
    # Invalid position
    if val < 0 or val > 4 or val >= info['numOfPos']:
        return -1
    angle = 0
    sign = 1
    if val == 1:
        angle = info['fiPos'][0]
    elif val == 2:
        angle = info['fiPos'][1]
        # Only the 4-way switch, which has specific characteristic
    elif val == 3:
        angle = info['fiPos'][0] * 3
    elif val == 4:
        angle = info['fiPos'][1] * 3
    if angle < 0:
        sign = -1
    # angle * steps form angle + which way to go * added Fi steps given by looseness of the switch grip
    # + steps Fi base
    return int(angle * stepsVar['stepsFromAngleBase']) + sign * stepsVar['stepsFiAdd'] + stepsVar['stepsFiBase']


## Convert steps axes to the matrix one
# @param stepsPos: Dictionary containing keys x, y, z and fi of steps from home position
# @param steps: Handler for the steps class
# @param glassShape: Glass shape dictionary containing the information about the HW state of system
# @return Dictionary containing keys x, y, z and fi of matrix position
def fromStepsToMatrix(stepsPos, steps, glassShape):
    ret = {'x': -1, 'y': -1, 'z': -1, 'fi': -1, 'state': -1}
    # Check if received steps has all the axes
    if ('x' not in stepsPos) or ('y' not in stepsPos) or ('z' not in stepsPos) or ('fi' not in stepsPos):
        print("The position has not all the values")
        return ret
    # Count the transformation to matrix position
    # Calculate the matrix position and shift direction for X axis
    ret['x'], stateX = countXPosition(stepsPos['x'], steps)
    # Calculate the matrix position and shift direction for Y axis
    ret['y'], stateY = countYPosition(stepsPos['y'], steps, glassShape)
    # Calculate Z position 0 - home, 1 - aligned with Z axis HW or 2 - pushing button
    ret['z'] = countZPosition(stepsPos['z'], steps)
    # Calculate Fi position 0 - upper one, 1 - left upper, 2 - right upper, 3 - left down or 4 - right down
    ret['fi'] = countFiPosition(stepsPos['fi'], steps)
    # Check if the shift is the allowed combination
    ret['state'] = 0
    # If the shift is in X axis and in Y axis together - not allowed combination
    if stateX != 0 and stateY != 0:
        ret = {'x': -1, 'y': -1, 'z': -1, 'fi': -1, 'state': -1}
    # Check shift in X axis
    elif stateX != 0:
        if stateX == 3 or stateX == 4:
            ret['state'] = stateX
        else:
            ret['state'] = -1
    # Check state in Y axis
    elif stateY != 0:
        if stateY == 1 or stateY == 2:
            ret['state'] = stateY
        else:
            ret['state'] = -1
    if not all(val >= 0 for val in ret.values()):
        ret = {'x': -1, 'y': -1, 'z': -1, 'fi': -1, 'state': -1}
    return ret


## From steps needed to be done from home position, calculates the matrix position in X axis
# @param val: Number of steps to be transformed
# @param steps: Handler to the steps class instance
# @return Position in matrix and state indicating the shift direction
def countXPosition(val, steps):
    if val == 0:
        # Home state
        return 0, 0
    stepsVar = steps.returnSteps()
    # Check if the position is invalid with the tolerance (0.1 mm)
    if val < 0 or val < stepsVar['startX'] - constants.FOUR_WAY_BUTTON_X * stepsVar['stepsFromMmBase'] - 10:
        # Undefined state
        return -1, -1
    # After eliminate possibilities of less then zero variant, the base value can be subtracted
    val = val - stepsVar['startX']
    # if the position is shifted to the left of X 1 position
    if val < 0:
        return 1, 3
    # Calculate the value of remaining steps after getting the matrix position (shift from it)
    fractal = val % stepsVar['stepsX']
    # Calculate to which position in matrix the steps should be (can be +1 if the shift is down)
    pos = 1 + val // stepsVar['stepsX']
    # The position is exactly on the middle of the matrix position with tolerance (0.1 mm)
    if fractal == 0 or (-10 <= fractal <= 10):
        state = 0
    # Shift left from the center with tolerance (0.1 mm), the matrix position is +1
    elif fractal >= stepsVar['stepsX'] - constants.FOUR_WAY_BUTTON_X * stepsVar['stepsFromMmBase'] - 10:
        pos = pos + 1
        state = 3
    # Check if the remaining possibilities are not out of right shift with tolerance (0.1 mm)
    elif fractal > constants.FOUR_WAY_BUTTON_X * stepsVar['stepsFromMmBase'] + 10:
        # Invalid state
        return -1, -1
    else:
        state = 4
    return pos, state


## From steps needed to be done from home position, calculates the matrix position in Y axis
# @param val: Number of steps to be transformed
# @param steps: Handler to the steps class instance
# @param glassShape: Glass shape dictionary containing the information about the HW state of system
# @return Position in matrix and state indicating the shift direction
def countYPosition(val, steps, glassShape):
    stepsVar = steps.returnSteps()
    # The max value is in [mm]
    maxShift = max(constants.FOUR_WAY_BUTTON_Y, constants.DOUBLE_BUTTON_Y)
    if val == 0:
        # Home state
        return 0, 0
    # Check if the position is invalid with the tolerance (0.1 mm)
    if val < 0 or val < stepsVar['startY'] - maxShift * stepsVar['stepsFromMmBase'] - 10:
        # Undefined state
        return -1, -1
    # After eliminate possibilities of less then zero variant, the base value can be subtracted
    val = val - stepsVar['startY']
    # if the position is shifted down of the Y max position
    if val < 0:
        return glassShape['matrixY'], 2
    # Calculate the value of remaining steps after getting the matrix position (shift from it)
    fractal = val % stepsVar['stepsY']
    # Calculate to which position in matrix the steps should be (can be +1 if the shift is down)
    pos = 1 + val // stepsVar['stepsY']
    # The position is exactly on the middle of the matrix position with tolerance (0.1 mm)
    if fractal == 0 or (-10 <= fractal <= 10):
        state = 0
    # Shift down from the matrix position with tolerance (0.1 mm), the matrix position is +1
    elif fractal >= stepsVar['stepsY'] - maxShift * stepsVar['stepsFromMmBase'] - 10:
        state = 2
        pos = pos + 1
    # Check if the remaining possibilities are not out of up shift with tolerance (0.1 mm)
    elif fractal > maxShift * stepsVar['stepsFromMmBase'] + 10:
        # Invalid state
        return -1, -1
    else:
        state = 1
    # Counting to the left upper corner to be (1,1)
    pos = (glassShape['matrixY'] - pos) + 1
    return pos, state


## From steps needed to be done from home position, calculates the matrix position in Z axis
# @param val: Number of steps to be transformed
# @param steps: Handler to the steps class instance
# @return Position (0 - home, 1 - aligned with Z axis HW or 2 - pushing button)
def countZPosition(val, steps):
    # Home state
    if val == 0:
        return 0
    stepsVar = steps.returnSteps()
    # Aligned position with Z axis HW with tolerance (0.1 mm)
    if val == stepsVar['startZ'] or (stepsVar['startZ'] - 10 <= val <= stepsVar['startZ'] + 10):
        return 1
    # After eliminate of home value or aligned position, the base value can be subtracted
    val = val - stepsVar['startZ']
    # min a max value is in [mm]
    minVal = min(constants.BUTTON_EXTENDED_DIST_Z, constants.BUTTON_FLUSH_DIST_Z, constants.DOUBLE_BUTTON_DIST_Z,
                 constants.FOUR_WAY_BUTTON_DIST_Z, constants.MUSHROOM_DIST_Z, constants.SWITCH_DIST_Z)
    maxVal = max(constants.BUTTON_EXTENDED_DIST_Z, constants.BUTTON_FLUSH_DIST_Z, constants.DOUBLE_BUTTON_DIST_Z,
                 constants.FOUR_WAY_BUTTON_DIST_Z, constants.MUSHROOM_DIST_Z, constants.SWITCH_DIST_Z)
    # Check if the position is in the interval of min and max value with some tolerance (0.1 mm)
    if minVal * stepsVar['stepsFromMmBase'] - 10 <= val <= maxVal * stepsVar['stepsFromMmBase'] + 10:
        return 2
    # Other cases are invalid
    return -1


## From steps needed to be done from home position, calculates the matrix position in Fi axis
# @param val: Number of steps to be transformed
# @param steps: Handler to the steps class instance
# @return Position (0 - upper one, 1 - left upper, 2 - right upper, 3 - left down or 4 - right down)
def countFiPosition(val, steps):
    if val < 0:
        return -1
    stepsVar = steps.returnSteps()
    # After eliminate possibilities of less then zero variant, the base value can be subtracted
    val = val - stepsVar['stepsFiBase']
    # Count position with some tolerance for the end click
    # plus tolerance 1.8°
    if val == 0 or (-stepsVar['stepsFiAdd']-2 <= val <= stepsVar['stepsFiAdd']+2):
        return 0
    # maxVal is in [mm]
    maxVal = max(constants.FOUR_WAY_FI, constants.MAINTAINED_FI, constants.MOMENTARY_FI)
    # Check if the value is in first or second quadrant possible positions with some tolerance for the end click
    # plus tolerance 1.8°
    if abs(val) <= maxVal * stepsVar['stepsFromAngleBase'] + stepsVar['stepsFiAdd'] + 2:
        # States 1 or 2
        if val > 0:
            return 1
        return 2
    # Only four-way switch
    # Check if the value is in first or second quadrant possible positions with some tolerance for the end click
    # plus tolerance 1.8°
    if abs(val) <= constants.FOUR_WAY_FI * 3 * stepsVar['stepsFromAngleBase'] + stepsVar['stepsFiAdd'] + 2:
        # States 3 and 4
        if val > 0:
            return 3
        return 4
    # Any other state
    return -1
