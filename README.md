# The PC part of the *Automation HW component tester*

This document is a user manual for the PC part of the Automation HW component tester. This part is written in Python 3.8. Its primary function is to evaluate camera output, calculate the CNC machine's trajectory and give user possibilities in the running test settings.

## Installation and prerequisites

For the python program, there are numerous packages imported. These packages need to be installed on the computer from which the application is running. The packages are:

- NumPy
- tk (Tkinter)
- threading
- time
- Pil (pillow)
- PySpin
- simple-pyspin
- spinnaker
- pyserial
- sys
- enum
- json
- queue
- math
- sklearn
- OpenCV built with TBB (to install it without TBB opencv-python this version could be slower than the TBB one)

Most of the packages are installed using:

`pip install <package>`

For PySpin, simple_pyspin and spinnaker, the Flir program Spinnaker needs to be installed. It can be downloaded on https://www.flir.com/products/spinnaker-sdk/. The version needs to be for Python, which is installed on the computer. For example, for Python 3.8, the **spinnaker_python-2.3.0.77-cp38-cp38-win32.zip** file needs to be downloaded. Follow the README inside the folder. Beware of the **win vs amd version**!

For installation of OpenCV with TBB, see the next chapter. 



### OpenCV build with TBB

For building OpenCV with the TBB option, the TBB package needs to be installed in the system as well as NumPy and matplotlib.

The guide for building with Cmake and Visual Studio can be found on https://docs.opencv.org/master/d3/d52/tutorial_windows_install.html section building the library.

When inserting the TBB path to the Cmake, the following directories and files need to be inserted:

| The TBB variable  | The path in the TBB directory        |
| :---------------- | ------------------------------------ |
| TBB_DIR           | */tbb/cmake                          |
| TBB_ENV_INCLUDE   | */tbb/include                        |
| TBB_ENV_LIB       | */tbb/lib/intel64/vc14/tbb.lib       |
| TBB_ENV_LIB_DEBUG | */tbb/lib/intel64/vc14/tbb_debug.lib |
| TBB_VER_FILE      | */tbb/include/tbb/tbb_stddef.h       |

Where * is the path to the TBB directory. 

If the system is not intel64, the appropriate system file needs to be inserted instead.

The OPENCV_PYTHON3_VERSION needs to be checked in the settings of Cmake too.

After successfully generating the Visual Studio solution, it needs to be opened, and the solution ALL_BUILD needs to be build from Visual Studio, followed by the INSTALL solution. 

For including the OpenCV to Python, the PATH to it and TBB needs to be set.

## The initialisation of the program

Some files need to be updated if any changes are done to the HW and the files containing the robot's commands if the terminal inserts are not used.

### Settings file

A **Settings.json** file contains the robot sizes, a number of rows and columns of the tested components' positions, and the space size between them.

In case of the change of the robot workspace, the file needs to be changed. There are four types of information:

1. **matrixX** and **matrixY**: This type of information holds the number of available locations for the components under test in horizontal (X) and vertical (Y) direction.
2. **sideX** and **sideY**: The horizontal (X) and vertical (Y) size of the plexiglass in mm.
3. **spaceX** and **spaceY**: The distance in mm between neighbouring centres of positions for the horizontal (X) and vertical (Y) direction components.
4. **firstX** and **firstY**: The space from the edge of the plexiglass to the centre of the left bottom position horizontal (X) and vertical (Y) in mm. 

### Steps file

This file holds the information about the number of steps needed in the X and Y position from the home position to the left bottom component position. ***Please do not change it by hand!*** The program changes it after the position adjustment explained in the section **Adjusting the position**. The adjusting needs to be done after changing or moving plexiglass or after changing the micro-steps settings.

### Program constants

In file **constants.py**, there are several constants inserted by hand. 

The file contains micro-steps settings, which needs to be changed in case of the motor drivers' change. **This change must be done in the PCB part too!**

Then, there are constants considering the components. They hold information about how much the button needs to be pushed or switch turned.  The data are inserted in mm or angles (°). These constants should not be changed unless the HW design is adjusted so that the distance from the Z-axis to the plexiglass changes or a new component is added to the list.

Another part of the constants holds the shift of the centre of the button for pushing in double and four-way buttons; this should not change.

There is a constant to calculate pixels from the mm information **MM_TO_PIXEL**; in case of the camera change, this needs to be changed.

If the different profiles for the attaching plexiglass are used, the **MM_IN_PROFILE** constant may need to be changed. The value is for both sides, so it is two times mm in profile.

For image recognition, there are also threshold values. In the case of camera change or the light around the robot, these values may differ, and it would be necessary to find new values to use.

### Configuration file

File holding the information about the components, their specification and position in the testing area. The specifications are separated by the end of the line, and one line consists of four parts separated by a comma. The line shape:

`x,y,<code>,<light colour>`

The **x** is a horizontal position in the matrix. It is the integer from 1 (left) to **matrixX** (right) (stored in the **Settings.json** file).

The **y** is a vertical position in the matrix. It is the integer from 1 (up) to **matrixY** (down) (stored in the **Settings.json** file). 

The **\<code\>** is the manufacturer code of the component placed on the given position. The component codes can be found in the [component specification](https://ecat.eaton.com/flip-cat/COMCON1_EN/blaetterkatalog/pdf/complete.pdf#page=21&zoom=130,317,840), and they hold information about the size and abilities of the component.

 The **\<light colour\>** is information about the light behind the component. It can be omitted or inserted as **RED** or **R**, **GREEN** or **G**, **BLUE** or **B** or **RGB**. The colour is not case sensitive, and the program will work fully without this information.

The **None** can be inserted to the **\<code\>** or the line can be omitted altogether if the place is empty.

The examples of valid configuration lines:

`1,1,None`

`2,1,M22-WRLK3-R,R`

`4,1,M22-DRLH-R-X0,None`

`6,1,M22-DP-G-X1`

A comma separates the parts of the line **without the white space!**

The configuration file is by default the **configuration.txt** file in the project folder. However, a different file can be put there as the argument of the program.

### Script (commands) file

A script file contains the commands for the robot separated by the end of the line. Recognised commands are explained in chapter **Commands**. 

The file is optional, and the commands can be inserted into the terminal or the GUI instead. The file can be put as the argument of the program or opened from the terminal or GUI.

## Running the program

Before starting the program, the virtual environment where the packages are installed needs to be activated.

The program is called from the project directory as:

`python3 main.py`

There are possible arguments to use while calling the program:

|          Argument          | Explanation                                                  |
| :------------------------: | ------------------------------------------------------------ |
|             -g             | Open the GUI version of the program                          |
|   -f \<file&nbsp;path\>    | System loads a file from the given path as a script file.    |
| -c&nbsp;\<file&nbsp;path\> | System loads configuration file from the given path as a configuration file. |

After the start, the program will do initialisation, which takes about a minute. It is initialised after "Camera Initialised" and "Threads are running" messages shows. If the camera is not connected, only the second message will show. Without the connection of the camera, some of the functions are limited. After the connection, the  "Camera Initialised" will show, and the functionality will be set.

If the camera will be disconnected while running, plug it in again and wait for the "Camera Initialised"  message.

### Before the run

Before the program run, there are few things, which needs to be done.

1. The Fi axis should be horizontal since the sensors for it are not part of the robot.
2. The motors should be powered up by plugging the power source into the wall socket.
3. The camera and the PCB should be connected to the computer with USB cables.
4. The camera should not have a lens cap on.

### Commands

The program recognises ten types of not case sensitive commands:

1. **connect \<port\>**

   Will connect to the given com port, if possible - for example, **COM4**. If the given comport is not available, it will inform the user.

2. **disconnect**

   Will disconnect from the port if the connection was established beforehand.

3. **exit**

   Will close the program.

4. **open \<file path\>**

   Opens the file from the path, if possible. Handles the lines from the script as separate commands as if they were typed to the terminal.

5. **home**

   Sends the HOME command to the control board to run the robot to the home position.

6. **push\<x,y\>** or **push\<x,y,state\>** or **switch\<x,y\>** or **switch\<x,y,state\>**

   The robot will push and release the button with the given state or turn the switch to the given state and release it in the given position. In the case of the omitted state, it is treated as 0.

7. **hold \<x,y\>** or **hold \<x,y,state\>**

   Will only push the button or turn the switch but will not release it.

8. **release**

   Will move the Z-axis back where it is not holding anything.

9. **light \<x,y\>**

   Return the state of the light indication. Returns **On** or **Off**, and in case of state On and the colour of the component cup white, it will also return the light's colour. For now, only Red, Green, Blue and White colours are supported.

10. **state \<x,y\>**

    Will return the state of the switch in which it is turned.

![](States.png)

The commands are always the keyword; if they have any arguments, they are separated from the keyword by space. A comma separates the individual arguments without space or any other character between them.

### Adjusting the position

For the adjustment, the graphical version of the program needs to be started. The image below shows the GUI without the camera connection.

![gui](Pictures/gui.png)

For the adjustment, the connection needs to be established. The right number of COMport can be obtained in the Windows device manager. In the selection, the correct number needs to be selected, and then the **Connect** button used. After the connection is established, the button will change to the Disconnect option.

The adjustment starts with the **Adjust home** button - it will go to the last saved position of the first left down component placement and change the button text to Homed possibility. 

The position can be adjusted by the arrows on the left side of the GUI. The X and Y-axis ones are for the adjustment. Forward and backward is supporting way so that the adjustment can be made in front of the component. The clockwise and anti-clockwise part can be used, but before hitting the **Homed** button, it needs to be in the horizontal position.

When the adjustment is made, the **Homed** button should be used, and the position is stored.

### Running the commands

When the first position is set for the current HW settings, the program can run tests.

There are three ways to do the commands described above. 

1. Use a terminal to type the commands there.
2. Use the right part of the GUI as a terminal.
3. Use script file - open it as an argument of the program or from the terminal.

### GUI

In the image above, the main window of the GUI is shown. There are three main parts in the window. The left one contains the computer's connection to the Control board and adjusting the position of the first component place.

The middle one shows the camera output if the camera is connected. To use light, state and switch commands, the camera needs to be connected and initialised.

The right part is the substitution for terminal communication. The commands can be typed there.

If there is any problem, the program will notify the user by the terminal or the pop-up window with the explanation.

## The inside working of the program

The program is split into five packages and the main part.

### The main part

This par is directly in the project file. There is the **main.py** file which is the script for running. Therefore the call of the program is done from this folder. All added files, for example, the configuration or script one, need to be inserted as a path from this directory. The main program calls all the threads and checks if they are running accordingly. It also calls the main window of GUI if the graphical version is called. 

There is also the **ComponentMatrix.py** file which contains the loaded information about the components placed on plexiglass. It loads the size of the components as well as its abilities. 

The **constants.py** file stores the constants for the program. It has information about the micro-steps settings, the light thresholds, the components size and other values.

In this directory, the **txt** files should be added, so the call is more comfortable. The **configuration.txt** contains the default settings of the components and the **script.txt** for the script calling instead of terminal use.

There is also the **Settings.json** file containing the plexiglass physical dimensions. And the **Steps.json** file, which stores the position of the first component for the current micro-steps settings and the plexiglass position.

### The packages

The directory then contains the subdirectories, which creates the packages of the program. There are five of them.

#### Communication

The communication directory contains the part which can establish the connection to the Control board. Then it can send and receive the messages from it. And parse the incoming messages to inform the other parts of the program of the progress.

This part also deals with the terminal's reading data and sends complete lines to the script parser.

There are four files for communication. The **serial_class.py** solves the connection to the serial port of the Control board. It is responsible for connection, disconnection, reading and writing the given message to it.

There is also the **COM.py** file, which solves the communication at a higher level. It has two threads: one for reading and one for writing. The reading one checks if there are any incoming messages, stores it in the buffer and invoke the parser. The writing part sends to the serial port the messages to be sent and stores the last one, so if the response does not come or it is the different one then the awaited one, it can send it again. It also has a timer to check the responses; if the COM port does not respond, it will disconnect the system and notify the user about the communication problem.

The **myParser.py** file deals with the incoming messages. It will parse the messages and send the reaction according to them. For example, if the response is the desired position, it will notify the system that the position was reached, and another command can be sent. In case of the problem, it  will invoke the process to correct the inconsistency if possible; otherwise, it will notify the user of the problem.

The last file **TerminalRead.py**, is there to read the user input from the terminal and store it to buffer until the end of a line is reached, then it sends it to the script parser to deal with.

#### GUI

The main window is created from the main.py.  However, some smaller parts of the graphical user interface are set aside to the GUI package.

The first file is the **ConnectionGUI.py** file which reads the list of available COM ports and connects the one user selects. It contains the drop-down selection of com ports, the button to refresh the list and the connect/disconnect button. The GUI part, which is handled by this file, is shown below.

|        Before connection         |            Connected            |
| :------------------------------: | :-----------------------------: |
| ![](Pictures/GUI_connection.png) | ![](Pictures/GUI_connected.png) |

The second part of the GUI package is the **HomeGUI.py** file which handles the position adjustment. There is the *Adjust home* button for the change from classical communication to the manual one. After the manual communication is activated, the button changes to *Homed*.  The arrows send the movement's direction to the Control board, and after the release, the event sends the Stop message. After every release, the Steps file is updated with the new value.

![](Pictures/GUI_homing.png)

There is also the *Home* button, which works only in the classical (not manual) mode, and it sends the robot to the left down position under the testing area and checks if the count is not amiss. If there is a problem, it will run the positioning using the proximity sensors.

The file **ImageGUI.py** shows the image from the camera and updates it periodically. It also resizes the image, showing the whole picture in case of resizing the window. It will show the biggest image which can fit the given space. If the camera is not connected, it will show the image displayed in the figure shown in the chapter: **Adjusting the position**.

The last part of the graphical interface package is the **TerminalGUI.py**. It creates the text widget with the  reading input and redirecting it to the parser thread. The output of the program is redirected there if the graphical version is called.

#### ImageProcessing

This package arranges the communication with the camera as well as the image recognition part. The file **cammera_communication.py** handles communication and settings. There is a possibility of setting the Region of interest, taking a picture with classical settings or the different exposure time, and reset the settings to work on automatic gain and exposure.

The second file is **process_image.py**, which arranges the recognition part. There are several aspects of image recognition. The camera is placed in a portrait direction, which means that any image returned from the camera is rotated by 90 degrees from the physical settings.  So the X and Y are switched, and the upper left corner of the image corresponds to the upper right one in the actual physical system.

##### Find the working area

When the camera is connected, the initialisation is done. The first part of the initialisation sets the camera settings to the default one - Region of interest to the whole sensor, the automatic settings of the gain and the exposure time. 

Then the image is taken, and the program will look for the lines represented by the physical boundaries - the profiles around the plexiglass. The program looks at the size given in the *Settings.json* file, then recalculates it to pixels and looks for the area big enough. If the carriage is in the camera way, it will filter out the lines.

|        Whole picture        |         Picture after ROI is set         |
| :-------------------------: | :--------------------------------------: |
| ![](Pictures/Img/whole.png) |       ![](Pictures/Img/croped.png)       |
|     **Edge detection**      |            **Line Detection**            |
| ![](Pictures/Img/edged.png) | ![](Pictures/Img/edged_lines_sorted.png) |



|  **Edge detection with a carriage**  |     **Line detection with a carriage**      |
| :----------------------------------: | :-----------------------------------------: |
| ![](Pictures/Img/edged_carriage.png) | ![](Pictures/Img/lines_carriage_colors.png) |

The tables above show steps between the recognition. It starts with gaussian blur and edge detection, followed by line detection with a certain length and a particular inclination. The green lines show the lines selected for the X (in the physical system Y) axis borders, and the yellow one the lines selected for the Y-axis (X in reality). The lines middles in the respective axis are there sorted into three groups by k-means algorithm. Then the system looks for the two groups, which satisfy the condition that the space between them is at least the plexiglass's size. 

##### Identify the component positions

After the ROI is set, the initialisation starts looking for the position of the components. It performs the circle detection algorithm from the OpenCV library and then sorts the circles. From the *Settings.json* file, the number of rows and columns is known. The information about the space form the side of the plexiglass is also inserted there. This information is used for the filtration of the data. The values too close to the image edge are thrown out.

|       Detected circles        |            After filtration            |
| :---------------------------: | :------------------------------------: |
| ![](Pictures/Img/circles.png) | ![](Pictures/Img/circles_filtered.png) |

The table above shows the detected circles, and the yellow ones are the ones, which are taken for further consideration. From the number of rows and columns, the k-means algorithm can be used for the X positions as well as the Y positions. From the groups, the average is calculated, and the raster of the component positions is created.

![](Pictures/Img/raster.png)

When the image of the component on the specific position is requested, the program takes an image with a size of the space between components (for example, calculates mm to pixels values y = 70mm and x = 50mm). The position from the raster is the middle of it.

##### Light on/off and colour recognition

The separate part of the program is the recognition of the light. The detection of the light can be a problem if the aperture is set, so position recognition is possible. That's why the change in exposure time is used. The classical settings can be seen in the figures above; the exposure time change is shown below. 

|   Exposure time 10 000 µs    |         Exposure time 5 000 µs          |
| :--------------------------: | :-------------------------------------: |
| ![](Pictures/Light/dark.png) | ![](Pictures/Light/dark_validation.png) |

For most of the light recognition, the 10 000 µs exposure time is used. The lower exposure would not allow detecting red switch light, sometimes other lights too. The exposure time of 5 000 µs is used when the program cannot say if the colour is one of the RGB or White one. The middle green light is the best example of the problem.

In most cases, the 10 000 µs image is taken from the camera, and it is cropped around the component position. The algorithm then works with the small cropped image. The program takes the image and converts it to HSV space; then, it filters out the values and saturation pixels lower than the threshold. Only enough coloured pixels will stay after the filtration. If the number of nonfiltered out pixels is higher than some threshold, the light is considered on. The image is converted back to RGB, the colour from the nonfiltered out pixels is averaged, and the higher value is considered as the returned colour. For now, it returns R, G or B colours. In the future, the RGB value can be returned, if needed - the change of code necessary.

However, this will filter out the white pixels. So the second filtration is done from the starting image, converted to greyscale, and the white pixels are counted (the value of pixel 255). The same threshold is applied to this count as in the colour version. The returned colour is White.

|              Component off               |              Component on               |               Color filtration                |              White filtration              |
| :--------------------------------------: | :-------------------------------------: | :-------------------------------------------: | :----------------------------------------: |
|  ![](Pictures/Light/button_red_off.png)  |  ![](Pictures/Light/button_red_on.png)  |  ![](Pictures/Light/button_red_filtered.png)  |  ![](Pictures/Light/button_red_white.png)  |
| ![](Pictures/Light/double_green_off.png) | ![](Pictures/Light/double_green_on.png) | ![](Pictures/Light/double_green_filtered.png) | ![](Pictures/Light/double_green_white.png) |
| ![](Pictures/Light/light_white_off.png)  | ![](Pictures/Light/light_white_on.png)  | ![](Pictures/Light/light_white_filtered.png)  | ![](Pictures/Light/light_white_white.png)  |
| ![](Pictures/Light/switch_blue_off.png)  | ![](Pictures/Light/switch_blue_on.png)  | ![](Pictures/Light/switch_blue_filtered.png)  | ![](Pictures/Light/switch_blue_white.png)  |

However, as mentioned above, the detection of coloured vs white light can be problematic if the light's intensity is too high. For example, in the table below, the problem is displayed. In the case of both colour and white detection cases, the lower exposure is taken to validate if the white is on or the colour one should be taken.


|       Exposure time 10 000 µs       |               Color filtration               |             White filtration              |          Exposure time 5 000 µs          |                White filtration                |
| :---------------------------------: | :------------------------------------------: | :---------------------------------------: | :--------------------------------------: | :--------------------------------------------: |
| ![](Pictures/Light/green_light.png) | ![](Pictures/Light/green_light_filtered.png) | ![](Pictures/Light/green_light_white.png) | ![](Pictures/Light/green_light_dark.png) | ![](Pictures/Light/green_light_dark_white.png) |

##### Switch position recognition

The last part of image recognition is switch position detection. As mentioned in the **Commands** chapter, switches have three states (2, 0, 1). Since the image is rotated by 90 degrees, 0 is the horizontal position, 2 points downwards and 1 upwards. 

The algorithm takes the component on the position. The algorithm takes the component on the position. Find the circle of the given size and crops it around it with some tolerance since the shadow can be detected as a circle (instead of the main component).

There was a problem with the recognition of the switch, which does not have a light on. It is a black pointer on a black circle. The result of edge detection is shown below.

|                Switch component                 |                       Cropped                        |               Edge detection               |
| :---------------------------------------------: | :--------------------------------------------------: | :----------------------------------------: |
| ![](Pictures/State/component_without_stamp.png) | ![](Pictures/State/component_crop_without_stamp.png) | ![](Pictures/State/edge_without_stamp.png) |

That is why the white markers are necessary. The switch has two parts of the thumb-grip version. The one with the translucent colour cup for the light and the second one is black - that is where the sticker needs to be added. 

With the sticker, edge detection shows some information about the direction. The algorithm then filters out most of the components' boundaries to reduce the possibility of detecting direction on the circle line instead of the component.

The line detection has two settings of the threshold. It uses the light detection algorithm to see if the component has a light on. If yes, then the line length is higher then for the light off. The algorithm progress is shown in the table below.

|              Component               |             Cropped             |              Edged              |             Filtered              |                  Lines                  |
| :----------------------------------: | :-----------------------------: | :-----------------------------: | :-------------------------------: | :-------------------------------------: |
|  ![](Pictures/State/component.png)   |  ![](Pictures/State/crop.png)   |  ![](Pictures/State/edge.png)   |  ![](Pictures/State/filter.png)   |  ![](Pictures/State/lines_detect.png)   |
| ![](Pictures/State/component_on.png) | ![](Pictures/State/crop_on.png) | ![](Pictures/State/edge_on.png) | ![](Pictures/State/filter_on.png) | ![](Pictures/State/lines_detect_on.png) |

After the lines are detected, the angle from the horizontal position is taken. The lines, which are out of some range are filtered out, then the average is taken, and the most divergent line is filtered out. The filtration continues until the number of lines is three or less, or the lines are all in some tolerance form the average. Sometimes the lines detected are in a different direction. If the lines are too different, the threshold is changed, and the algorithm tries to find the position again.

With the averaged angle from the horizontal position, the state's desition (position of the switch) is done. If there is any problem during the recognition, the algorithm tries to take another picture and repeat the recognition three times before informing the user about the failure.

#### Parser

The parser has two files. One file parses the configuration file, and the second the commands or the script file.

##### Configuration file parsing

The configuration file parser **parseConfig.py** opens the **configure.txt** file or the file given from the program parameter and splits it by the lines. The comma separates every line to the position, component code and the light colour.

The component parameters are added to the component matrix so that the other parts of the program can use the information comfortably. 

For now, the components supported are Button, Switch and Light. For the switch, only the thumb-grip version is supported since another switching tool would be necessary for the different head. However, the mechanism behind it is the same, so the tested part is not changed. The thumb-grip version was selected since it also supports light.

The picture below shows the button component decoding.

 ![](Pictures/Components/button_diagram.png)

For the switch component, the diagram is shown below:

![](Pictures/Components/switch_diagram.png)

And the light one:

![](Pictures/Components/light_diagram.png)

The information about the component stored in the component matrix is stored in the dictionary containing the parts: 

1. **numOfPos**: number of the position which can be pushed or rotated to (for light, the information is 0).
2. **distanceInZ**: It is specific to the component - the info about the mm is in the **constants.py**.
3. **xPos**: Tupple storing the shift in the x-axis in mm to the left and the right (for the 4-way button).
4. **yPos**: Tupple storing the shift in the y-axis in mm up and down (for double buttons and 4-way button).
5. **fiPos**: The switch's angle can be turned (60 or 40 degrees for different types), tot he left and to the right.
6. **light**: Information if the component can have a light indication.
7. **lightColor**: Information about the light's colour - it is currently not used, the input is not needed.
8. **type**: Enum **HwTypes** storing the component type - the enum can be found in the file **ComponentMatrix.py**.
9. **cupColor**: Information about the cup - only for the white ones, the program returns the light's colour.
10. **state**: Store the information about the switch position. After the initialisation (detect all from the image), it stores the information and updates it after the change or after the detection from the image.
11. **locking**: Enum **LockingType** stores information about the maintained or momentary types or any combinations. The enum can be found in the file **ComponentMatrix.py**.

##### Parsing of the commands

The file **ParseScript.py** runs a thread for the parsing. Other parts of the program, which reads from a terminal or GUI output, put the input to the buffer, and the parser goes through it in its thread. The commands supported are listed in the **Commands** chapter. 

This part of the script splits the input by the lines. Then they are divided by the space since the command, and the parameters are separated by it. The comma then splits the parameters. The parameters are checked if they point to the existing position, and the component on the position supports the given command. The scripts call the motions by writing to the Control board or asking the image recognition for the information about the component.

#### Position

This package has two files. The first file is **CurrentPosition.py**. The file stores the last returned position form the Control board to the directory; other parts of the program can use it by getters and setters.

The file also stores the last asked position so that the incoming information can be compared to the asked one, and the appropriate action can be made if it is different.

The second file, **transformPosition.py,** takes the matrix shape position and returns the position in the control board's steps and via versa.