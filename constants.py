import json


# The state of drivers
MICRO_STEPS = 8
MICRO_STEPS_FI = 2

## Distances from Z0 edge in [mm] for pushing the button or switching the switch
BUTTON_EXTENDED_DIST_Z = 45
BUTTON_FLUSH_DIST_Z = 51.5
MUSHROOM_DIST_Z = 39.5
DOUBLE_BUTTON_DIST_Z = 49.5
FOUR_WAY_BUTTON_DIST_Z = 50
SWITCH_DIST_Z = 43

## Angle of the switches in [°]
MAINTAINED_FI = 60
MOMENTARY_FI = 40
FOUR_WAY_FI = 45

## Shift of the active button part from the center of part
DOUBLE_BUTTON_Y = 14
FOUR_WAY_BUTTON_X = 14
FOUR_WAY_BUTTON_Y = 14

## The transformation for image recognition
MM_TO_PIXEL = 1.23
## How many mm is the plexiglass inserted into profile (assuming it is inserted on both sides)
MM_IN_PROFILE = (5*2)

## Threshold for the saturation and value in case of buttons, lights and other type components
LIGHT_TRASH = 145
## Minimum number of pixels, which needs to be detected after threshold
LIGHT_MIN_PIXELS = 110
## Threshold for the saturation and value in case of switch type components
LIGHT_THRESH_SWITCH = 130
## Minimum number of pixels, which needs to be detected after threshold in case of switch type component
LIGHT_MIN_PIXELS_SWITCH = 35


## Class for holding steps variables
#
# Class will create dictionary for storing steps variables
# The variables will be read  from json file, or calculated from distances given
class Steps:
    ## Init function for Steps class
    # @param settings: Handler of the settings class
    def __init__(self, settings):
        ## Dictionary variable to store steps information
        self.stepsVar = {}
        ## Handler of the settings class
        self.settings = settings
        self.fillStructure()

    ## Fill the structure with values in the json and calculate the values depending on settings
    def fillStructure(self):
        shape = self.settings.getGlassShape()
        self.readFileSteps()
        ## Constant given by construction
        self.stepsVar['startZ'] = 1625 * MICRO_STEPS
        ## Values given by settings distances
        self.stepsVar['stepsX'] = shape['spaceX'] * 50 * MICRO_STEPS
        self.stepsVar['stepsY'] = shape['spaceY'] * 50 * MICRO_STEPS
        ## Values bases for variables differ by Hw - multiply it by [mm] or [°]
        self.stepsVar['stepsFromMmBase'] = 50 * MICRO_STEPS
        # Floor the multiplied value
        self.stepsVar['stepsFromAngleBase'] = 1.111
        ## Additional constant for Fi given by looseness of the switch grip
        self.stepsVar['stepsFiAdd'] = 10
        ## Considering horizontal position of tool homed
        self.stepsVar['stepsFiBase'] = 100 * MICRO_STEPS_FI

    ## Read the values in the json file
    def readFileSteps(self):
        with open("Steps.json", "r") as read:
            self.stepsVar = json.load(read)

    ## Update the json file with new variables
    def updateFileSteps(self):
        ## The user rewritable variables - in gui or in the json
        stepsWrite = {
            "startX": self.stepsVar['startX'],
            "startY": self.stepsVar['startY']
        }
        with open("Steps.json", "w") as write:
            json.dump(stepsWrite, write)

    ## Give dictionary of steps variables
    # @return steps variables
    def returnSteps(self):
        return self.stepsVar

    ## Write the value to key in steps variables
    # @param key: Key to which the variable should be set
    # @param val: Value which should be set
    def writeSteps(self, key, val):
        if key in self.stepsVar.keys() and val > 0:
            self.stepsVar[key] = val
        else:
            print("Given key is not in the steps variables.")

    ## Recalculate the steps depending on settings
    def updateSteps(self):
        shape = self.settings.getGlassShape()
        ## Values given by distances
        self.stepsVar['stepsX'] = shape['spaceX'] * 50 * MICRO_STEPS
        self.stepsVar['stepsY'] = shape['spaceY'] * 50 * MICRO_STEPS


## Class storing the user given settings
#
# Store, load and update real state given values about the Plexiglas
class Settings:
    ## Init function of settings class
    def __init__(self):
        ## Variable storing dictionary of glass sizes and spaces between places for HW
        self.glassShape = {}
        self.readFileSettings()

    ## Read the settings from json file
    def readFileSettings(self):
        with open("Settings.json", "r") as read:
            self.glassShape = json.load(read)

    ##  Writes to the json file the current settings
    def updateFileSettings(self):
        with open("Settings.json", "w") as write:
            json.dump(self.glassShape, write)

    ## Update the variable given
    # @param key: Key to the variable to change
    # @param value: Value to assign
    def updateSettings(self, key, value):
        if key in self.glassShape.keys() and value > 0:
            self.glassShape[key] = value
            self.updateFileSettings()
        else:
            print("Given key is not in the distances variables.")

    ## Give all variables of Plexiglas distances
    # @return glassShape dictionary variable
    def getGlassShape(self):
        return self.glassShape
