import tkinter as tk
import Communication.serial_class as ser_class
import Communication.myParser as myParser
import Communication.COM as COM
import Communication.TerminalRead as terRead
import ImageProcessing.camera_comunication as cam_cmm
import ImageProcessing.process_image as proc_imm
import GUI.ImageGUI as ImageGUI
import GUI.ConnectionGUI as ConnGUI
import GUI.TerminalGUI as TerminalGUI
import GUI.HomeGUI as HomeGUI
import constants
import ComponentMatrix
import Parser.ParseConfig as ParseConfig
import Position.CurrentPosition as CurrPos
import sys
import Parser.ParseScript as ScriptParse

## Global variable to indicate that the program should end
exit_program = False


## Return the value of global variable exit_program
# @return exit_program value
def getExit():
    global exit_program
    return exit_program


## Write to the global variable exit_program
# @param val: value to write - True or False
def writeExit(val):
    global exit_program
    exit_program = val


# Copied from:
# https://stackoverflow.com/questions/20303291/issue-with-redirecting-stdout-to-tkinter-text-widget-with-threads
## Class for redirecting the output to the text widget
class StdRedirect:
    ## Initialisation function for redirecting output messages
    # @param widget: Handler for text widget where the output should be redirected
    def __init__(self, widget):
        ## Handler for text widget where the output should be redirected
        self.widget = widget

    ## Write to widget
    # @param string: String to write
    def write(self, string):
        self.widget.write(string)

    ## Needed flush function without implementation
    def flush(self):
        pass


## Routine to handle exit of the program
# @param cam: Camera connection handler
# @param read_thread: Reading thread handler
# @param write_thread: Write thread handler
# @param parse_thread: Parsing thread handler
# @param serial_handle: Serial communication handler
# @param readTerminal: Terminal reading handler
def exitRoutine(cam, read_thread, write_thread, parse_thread, serial_handle, readTerminal=None):
    # See if the read thread is running, if yes then stop it
    if read_thread.is_alive():
        read_thread.stop_thread()
    # See if the write thread is running, if yes then stop it
    if write_thread.is_alive():
        write_thread.stop_thread()
    # See if parsing thread is running, if yes then stop it
    if parse_thread.is_alive():
        parse_thread.stop_thread()
    # Disconnect the serial port communication
    serial_handle.disconnect()
    # End the GUI
    writeExit(True)
    # Close video
    if cam is not None and cam.isConnected:
        cam.closeCamera()
    # If the read from terminal thread is running, stop it
    if readTerminal is not None:
        readTerminal.stop_thread()


## Starting point of the program
# @param argv: commandline start-up argument
def main(argv):
    isGraphical = 0
    # Default file name - assuming it is inserted to the program directory
    file = "configure.txt"
    scriptFile = None
    # Starting size of the program window, in case of GUI
    startWidth = 1000
    startHeight = 580
    # Set the value for exit program to False
    writeExit(False)
    # If there is any argument except of script name
    if len(argv) > 1:
        for arg in argv:
            if arg == "-g":
                # The program should run in GUI version
                isGraphical = 1
            elif arg == "-f" or arg == "--file":
                # The script file name was inserted
                next_index = argv.index(arg) + 1
                if len(argv) > next_index:
                    scriptFile = argv[next_index]
            elif arg == "-c" or arg == "--config":
                # The configuration file name was inserted
                next_index = argv.index(arg) + 1
                if len(argv) > next_index:
                    file = argv[next_index]
    # Read the settings of the program
    settings = constants.Settings()
    # Read the starting point of steps
    steps = constants.Steps(settings)
    # Create component matrix
    matrix = ComponentMatrix.ComponentMatrix(settings)
    # Parse the configuration file and fill the component matrix
    parseConf = ParseConfig.ParseConfig(settings, matrix, file)
    if not parseConf.configured:
        # The configuration file could not be read - the system does not have information needed for continuing
        writeExit(True)
    # Initialise current position storage
    position = CurrPos.CurrentPosition(settings)

    if isGraphical:
        # The main GUI object handler
        master = tk.Tk()
        master.title("Automatic HW component tester")
        # Size of the window at the beginning
        master.geometry("%dx%d+0+0" % (startWidth, startHeight))
        master.minsize(580, 580)
        # Configuration of the grid
        for column in range(2):
            master.columnconfigure(column, weight=1, minsize=200)
        for row in range(4):
            master.rowconfigure(row, weight=1)
        master.columnconfigure(0, weight=0)
        master.rowconfigure(1, weight=1, minsize=100)
    else:
        master = None

    # Prepare the serial connection
    ser = ser_class.SerialPort()
    # Create camera communication
    camera = cam_cmm.CameraCmm()
    # Create the image processing object
    procImg = proc_imm.ProcessImg(camera, settings)
    cameraWasConnected = False
    if camera.isConnected == 1:
        procImg.initWithCamera(matrix)
        cameraWasConnected = True
        print("Camera Initialised")

    # Prepare serial communication threads
    try:
        # The writing part of communication thread handler
        write = COM.CmmSend(ser)
        write.daemon = True
        write.start()
        parse = myParser.ParserClass(write, position, settings, steps)
        # The read thread handler
        read = COM.ReadThread(ser, parse)
        read.daemon = True
        read.start()
        # The parse script thread handler
        scriptParse = ScriptParse.ScriptParser(ser, settings, steps, matrix, write, position, procImg)
        scriptParse.daemon = True
        scriptParse.start()
        # The reading from terminal, if the GUI is not running
        if not isGraphical:
            readTerminal = terRead.TerminalRead(scriptParse)
            readTerminal.daemon = True
            readTerminal.start()
        else:
            readTerminal = None
        print("Threads are running.")
    except Exception as e:
        print("Could not create the threads")
        print("Exception: ", e)
        read = None
        write = None
        scriptParse = None
        readTerminal = None
        exit(1)

    if isGraphical:
        # Create gui for connection
        conn = ConnGUI.ConnectionGUI(master, read, write, ser, write)
        conn.grid(row=0, column=0, padx=1, pady=1, sticky="NSEW")
        # Create the gui for homing
        home = HomeGUI.HomeGUI(master, write, conn, settings, steps, position)
        home.grid(row=1, column=0, padx=1, pady=1, sticky="NSEW")
        # Create gui for displaying the image
        image = ImageGUI.ImageGUI(master, startWidth, startHeight, camera)
        image.grid(row=0, column=1, rowspan=5, sticky="NSEW", padx=1, pady=1)
        # Create gui for terminal
        terminal = TerminalGUI.TerminalGUI(master, scriptParse)
        terminal.grid(row=0, column=2, rowspan=4, padx=1, pady=1)
        # Change the stdout to terminal in GUI
        sys.stdout = StdRedirect(terminal)
        # Set exit routine for the program
        master.protocol('WM_DELETE_WINDOW', lambda: exitRoutine(camera, read, write, scriptParse, ser))
    else:
        conn = None
        image = None

    # If script file was inserted as parameter, read it for parsing
    if scriptFile is not None:
        scriptParse.setScript(f"open {scriptFile}")

    wasConnected = 0
    # Start the main loop
    while not exit_program:
        if camera.problemInformed:
            # Camera initialisation problem - could not continue
            exitRoutine(camera, read, write, scriptParse, ser, readTerminal)
            break
        # Find out if the camera is initialised
        if camera.isConnected == 0:
            if cameraWasConnected is True:
                print("Camera was disconnected")
                procImg.isInitialised = False
                cameraWasConnected = False
            if camera.connect() and procImg.isInitialised is False:
                procImg.initWithCamera(matrix)
                cameraWasConnected = True
                print("Camera Initialised")
        elif camera.isConnected and procImg.isInitialised is False:
            procImg.initWithCamera(matrix)
            cameraWasConnected = True
            print("Camera Initialised")
        # If script ended, the program should be exited
        if not scriptParse.is_alive():
            exitRoutine(camera, read, write, scriptParse, ser, readTerminal)
            break
        # In case of graphical version, update the image and the master window
        if isGraphical:
            master.update()
            image.updateImage(0)
        # Check if there are any changes in the serial connection
        if ser.isConnected():
            # The connection was established from terminal - if the gui is running, need to update it
            if wasConnected == 0 and isGraphical:
                conn.connectionStateChange(True)
            wasConnected = 1
        elif wasConnected and not ser.isConnected():
            # The system wos disconnected
            if isGraphical:
                conn.connectionStateChange(False)
            wasConnected = 0


if __name__ == "__main__":
    main(sys.argv)
